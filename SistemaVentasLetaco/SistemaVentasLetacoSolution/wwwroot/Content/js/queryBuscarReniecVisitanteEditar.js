﻿$(function () {

    var table = {}

    $("#doc_num_vis").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $('#modalBusquedaReniec').on('hidden.bs.modal', function (e) {
        $(this).find("input,textarea,select").val('').end().find("input[type=checkbox], input[type=radio]").prop("checked", "").end();
        $("#dataTableMvc tbody tr").remove();
        $("#dataTableMvc_info").html("");
        $("#dataTableMvc_paginate").html("");
    });

    $("#btnBuscarReniec").on("click", function () {
        dtCargarDatos();
        debugger;
    });

    function dtCargarDatos() {
        debugger;
        var parametros = {
            tipoConsulta: "0",
            dni: $("#idNumDocumentoReniec").val(),
            apePaterno: $("#idApePaterno").val(),
            apeMaterno: $("#idApeMaterno").val(),
            nombres: $("#idNombres").val()
        }

        table = $("#dataTableMvc").DataTable({
            destroy: true,
            responsive: true,
            ajax: {
                method: "POST",
                url: "http://localhost:39327/Visitante/BuscarEnReniec",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: function (d) {
                    d.parametros = parametros;
                    return JSON.stringify(d);
                },
                dataSrc: function (d) {
                    return d.data;
                }
            },
            "language":
            {
                "processing": "Refrescando data...",
                "lengthMenu": "items x pág. _MENU_",
                "zeroRecords": "No se encontraron registros.",
                "info": "Mostrando del _START_ al _END_ de _TOTAL_ registros en total",
                "infoEmpty": "No hay registros.",
                "infoFiltered": "",
                "search": "",
                "paginate": {
                    "first": "<span class=\"icon fa fa-angle-double-left\"></span>",
                    "previous": "<span class=\"icon fa fa-angle-left\"></span>",
                    "next": "<span class=\"icon fa fa-angle-right\"></span>",
                    "last": "<span class=\"icon fa fa-angle-double-right\"></span>"
                }
            },
            "sDom": ' t <"padding-xs-hr" <"table-caption no-padding"> <"clear"T> <"DT-lf-right"> > t <"table-footer padding-xs-hr clearfix" <"DT-label"i> <"DT-pagination"p> >',
            "oTableTools": {
                "aButtons": [
                ],
                "sSwfPath": "${pageContext.request.contextPath}/assets/libs/DataTables-1.10.2/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
            },

            columns: [
                { "data": "NumeroDNI" },
                { "data": "ApellidoPaterno" },
                { "data": "ApellidoMaterno" },
                { "data": "PreNombres" },
                {
                    "defaultContent": "<button type='button' id='verDetallesReniec' name='verDetallesReniec' class='btn btn-info'>Ver Detalle</button>"
                },
                {
                    "defaultContent": "<button type='button'  class='btn btn-danger'>Recuperar</button>"
                }
            ]
        });

        $('#dataTableMvc tbody').on('click', 'button.btn.btn-danger', function () {
            var data = table.row($(this).parents('tr')).data();
            var parametros = {
                vis_nom: data.PreNombres,
                vis_ape_pat: data.ApellidoPaterno,
                vis_ape_mat: data.ApellidoMaterno,
                sex_id: 1,
                doc_num_vis: data.NumeroDNI,
                int_id_ref: $("#int_id_ref").val()
            }
            //data.creacion = parametros;
            //var url = 'http://localhost:39327/Visitante/CreateModel/';
            //url += "?dni=" + parametros.doc_num_vis;
            //url += "&paterno=" + parametros.vis_ape_pat;
            //url += "&materno=" + parametros.vis_ape_mat;
            //url += "&nombre=" + parametros.vis_nom;
            //url += "&id_ref=" + parametros.int_id_ref;
            //window.location.href = url;
            //var parametros = {
            //    vis_nom: $("#Nombres").val(),
            //    vis_ape_pat: $("#ApellidoPaterno").val(),
            //    vis_ape_mat: $("#ApellidoMaterno").val(),
            //    sex_id: $("#Sexo").val(),
            //    doc_num_vis: $("#NumeroDNI").val(),
            //    int_id_ref: $("#int_id_ref").val(),
            //    vis_id: $("").val()
            //}
            $("#vis_nom").val(parametros.vis_nom);
            $("#vis_ape_pat").val(parametros.vis_ape_pat);
            $("#vis_ape_mat").val(parametros.vis_ape_mat);
            $("#sexo_id").val(parametros.sex_id);
            $("#doc_num_vis").val(parametros.doc_num_vis);         
        });

        $('#dataTableMvc tbody').on('click', 'button.btn.btn-info', function () {
            var data = table.row($(this).parents('tr')).data()
            var parametros = {
                tipoConsulta: "0",
                dni: data.NumeroDNI,
                apePaterno: "",
                apeMaterno: "",
                nombres: ""
            }
            data.parametros = parametros;
            $.ajax({
                method: "POST",
                url: "http://localhost:39327/Visitante/verDetalleReniec",
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                data: JSON.stringify(data),
                success: function (response) {
                    console.log(response);
                    $("#partialModal").html(response);
                    $("#modalPartial").modal('show');
                }
            });

        });

    }

})

function verDetallesReniec() {
    debugger;
    var data = {};
    var parametros = {
        tipoConsulta: "0",
        dni: $("#idNumDocumentoReniec").val(),
        apePaterno: "",
        apeMaterno: "",
        nombres: ""
    }
    data.parametros = parametros;
    $.ajax({
        method: "POST",
        url: "http://localhost:39327/Visitante/verDetalleReniec",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify(data),
        success: function (response) {
            console.log(response);
            $("#partialModal").html(response);
            $("#modalPartial").modal('show');
        }
    });
};

$("#verDetallesReniec").click(verDetallesReniec);