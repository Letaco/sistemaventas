﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaVentasLetacoSolution.ViewModels.Sistema.Seguridad
{
    public class SessionPermisoViewModel
    {
        public int USUARIO_ID { get; set; }
        public String NOMBRES_USUARIO { get; set; } = "";
        public String USUARIOLOGEO { get; set; } = "";
        public int SISTEMA_ID { get; set; } = -1;
        public String KEY_SISTEMA { get; set; } = "";
        public bool RESET_CLAVE { get; set; } = false;
        public String ERROR_SMS { get; set; } = "";
        public DateTime FECHA_HORA_INICIO { get; set; } = DateTime.Now;
        public String LOGIN { get; set; } = "";
        public String DESCRIPCION_CARGO { get; set; }
        public String TIPO_CARGO { get; set; }
        public String NUMERO_TELEFONO { get; set; }
        public long token { get; set; } = -1;
        public String ArbolMenuSistema { get; set; }
        public int TRABAJADOR_ID { get; set; }
    }
}
