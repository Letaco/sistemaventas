﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaVentasLetacoSolution.ViewModels
{
    public class LoginSesionViewModel
    {
        public string ERROR_SMS { get; set; }
        public int USUARIO_ID { get; set; }
        public String LOGIN { get; set; }
        public String PASSWORD { get; set; }
        public String NOMBRES_USUARIO { get; set; }
        public DateTime FECHA_HORA_INICIO { get; set; }
        public int SISTEMA_ID { get; set; }
        public String SISTEMA_KEY { get; set; }
    }
}
