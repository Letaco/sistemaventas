﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.ComponentModel.DataAnnotations;
using LogicaNegocio;
using Entidades;
using System.Net;

namespace SistemaVentasLetacoSolution.ViewModels.RecursosHumanos
{
    public class RecursosHumanosViewModel
    {
        [Display(Name = "Número de DNI:")]
        public String dni { get; set; }

        [Display(Name = "Nombres:")]
        public String Nombres { get; set; }

        [Display(Name = "Apellido Paterno:")]
        public String Paterno { get; set; }

        [Display(Name = "Apellido Materno:")]
        public String Materno { get; set; }

        [Display(Name = "Fecha de Nacimiento:")]
        public string FechaNacimiento { get; set; }

        [Display(Name = "Departamento:")]
        public string DepartamentoNombre { get; set; }

        [Display(Name = "Provincia:")]
        public string ProvinciaNombre { get; set; }

        [Display(Name = "Distrito:")]
        public string DistritoNombre { get; set; }

        [Display(Name = "Dirección:")]
        public string Direccion { get; set; }

        [Display(Name = "Nombre Completo:")]
        public String NombreCompleto { get; set; }

        [Display(Name = "Usuario:")]
        public String Usuario { get; set; }

        [Display(Name = "Contraseña:")]
        public String Contrasena { get; set; }

        [Display(Name = "Fecha de Ingreso:")]
        public string FechaIngreso { get; set; }

        [Display(Name = "Sucursal:")]
        public string Sucursal { get; set; }

        [Display(Name = "Cargo:")]
        public string Cargo { get; set; }

        [Display(Name = "Tipo Doc. Identidad")]
        public int TipoDocumentoId { get; set; }

        [Display(Name = "Tipo Doc. Identidad")]
        public int idTipoDocIdentidad { get; set; }  

        public string Foto_64bits { get; set; } = "";

        public CookieContainer myCookie { get; set; }

        public byte[] FotoPerfilfrente_Bytes
        {
            get
            {
                return Convert.FromBase64String(Foto_64bits);
            }
        }

    }
}
