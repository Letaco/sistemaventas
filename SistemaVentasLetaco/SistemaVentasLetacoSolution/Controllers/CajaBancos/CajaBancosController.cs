﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SistemaVentasLetacoSolution.Controllers.CajaBancos
{
    public class CajaBancosController : Controller
    {
        // GET: CajaBancos
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TipoCaja()
        {
            return View();
        }

        public ActionResult MantBanco()
        {
            return View();
        }
        public ActionResult MantCaja()
        {
            return View();
        }

        public ActionResult CuentaBancaria()
        {
            return View();
        }

        public ActionResult AsigEmpleadoCaja()
        {
            return View();
        }

        public ActionResult AperturaCierreCaja()
        {
            return View();
        }

        public ActionResult HistorialCaja()
        {
            return View();
        }

        // GET: CajaBancos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CajaBancos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CajaBancos/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CajaBancos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CajaBancos/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CajaBancos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CajaBancos/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}