﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Entidades;
using EnlacePresentacion;
using EntidadesIntermedias;
using ServiciosConsultaLetaco;
using Newtonsoft.Json;
using System.Net;
using System.Drawing;
using System.IO;

namespace SistemaVentasLetacoSolution.Controllers
{
    public class RecursosHumanosController : Controller
    {
        // GET: RecursosHumanos
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Persona()
        {
            return View();
        }

        public ActionResult RegistrarPersona()
        {
            return View();
        }

        public ActionResult Contrato()
        {
            return View();
        }

        public ActionResult HistorialContrato()
        {
            return View();
        }

        [HttpPost]
        public JsonResult InsertaPersona(string nombresRazSocial, string apellidosNomComp, DateTime fecNacIniActv, int idTipoPersona,
            string imagen, int esbanco, int escliente, int esproveedor, int escolaborador, int esafp, int esfamiliar, int essocio)
        {
            ENT_Persona datosPersona = new ENT_Persona();
            datosPersona.nombres_razSoc = nombresRazSocial;
            datosPersona.apellidos_nomCom = apellidosNomComp;
            datosPersona.fechaActv = fecNacIniActv;
            datosPersona.idTipoPersona = idTipoPersona;
            datosPersona.esBanco = esbanco;
            datosPersona.esCliente = escliente;
            datosPersona.esProveedor = esproveedor;
            datosPersona.esColaborador = escolaborador;
            datosPersona.esAFP = esafp;
            datosPersona.esFamiliar = esfamiliar;
            datosPersona.esSocio = essocio;
            datosPersona.fotoPersona = imagen;
            datosPersona.idUsuaReg = 1;//Reemplazar por dato de sesión

            PersonaRequest personaRequest = new PersonaRequest();
            personaRequest.eNT_Persona = datosPersona;

            PersonaResponse personaResponse = new PersonaResponse();

            Respuesta respuestaTransaccion;

            try
            {
                EPBL_Persona ePBL_Persona = new EPBL_Persona();

                personaResponse = ePBL_Persona.Insertar(personaRequest);

                respuestaTransaccion = personaResponse.respuestaTransaccion;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = "Error - " + ex.Message };
            }

            return Json(JsonConvert.SerializeObject(respuestaTransaccion));
        }

        public ActionResult Colaborador()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarPersonaCombo(int tipoListado)
        {
            PersonaRequest personaRequest = new PersonaRequest();
            personaRequest.tipoListado = tipoListado;

            PersonaResponse personaResponse = new PersonaResponse();

            Respuesta respuestaTransaccion;

            try
            {
                EPBL_Persona ePBL_Persona = new EPBL_Persona();

                personaResponse = ePBL_Persona.ListarPersonaCombo(personaRequest);

                respuestaTransaccion = personaResponse.respuestaTransaccion;
            }
            catch (Exception ex)
            {
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            personaResponse.respuestaTransaccion = respuestaTransaccion;

            return Json(JsonConvert.SerializeObject(personaResponse));
        }

        public ActionResult Usuario()
        {
            return View();
        }

        //[HttpPost]
        //public JsonResult InsertarUsuario(string usuario, string clave, string pregunta, string respuesta, int idpersona)
        //{
        //    ENT_Usuario datosUsuario = new ENT_Usuario();
        //    datosUsuario.usuario = usuario;
        //    datosUsuario.clave = encriptarClave(clave);
        //    datosUsuario.preguntaSeguridad = (string.IsNullOrEmpty(pregunta)) ? string.Empty : encriptarClave(pregunta);
        //    datosUsuario.respuesta = (string.IsNullOrEmpty(respuesta)) ? string.Empty : encriptarClave(respuesta);
        //    datosUsuario.idPersona = idpersona;
        //    datosUsuario.idUsuaReg = 1;//Reemplazar por dato de sesión

        //    UsuarioRequest usuarioRequest = new UsuarioRequest();
        //    usuarioRequest.eNT_Usuario = datosUsuario;

        //    UsuarioResponse usuarioResponse = new UsuarioResponse();

        //    Respuesta respuestaTransaccion;

        //    try
        //    {
        //        EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();

        //        usuarioResponse = ePBL_Usuario.Insertar(usuarioRequest);

        //        respuestaTransaccion = usuarioResponse.respuestaTransaccion;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        respuestaTransaccion = new Respuesta() { p_idRespuesta = -1, p_mensajeRespuesta = "Error - " + ex.Message };
        //    }

        //    return Json(JsonConvert.SerializeObject(respuestaTransaccion));
        //}

        //public JsonResult EditarUsuario(int idUsuario, string usuario, string clave, string pregunta, string respuesta, int idpersona, int estado)
        //{
        //    ENT_Usuario datosUsuario = new ENT_Usuario();
        //    datosUsuario.idUsuario = idUsuario;
        //    datosUsuario.usuario = usuario;
        //    datosUsuario.idPersona = idpersona;
        //    datosUsuario.clave = encriptarClave(clave);
        //    datosUsuario.preguntaSeguridad = (string.IsNullOrEmpty(pregunta)) ? string.Empty : encriptarClave(pregunta);
        //    datosUsuario.respuesta = (string.IsNullOrEmpty(respuesta)) ? string.Empty : encriptarClave(respuesta);            
        //    datosUsuario.estado = estado;
        //    datosUsuario.idUsuActv = 1;//Reemplazar por dato de sesión

        //    UsuarioRequest usuarioRequest = new UsuarioRequest();
        //    usuarioRequest.eNT_Usuario = datosUsuario;

        //    UsuarioResponse usuarioResponse = new UsuarioResponse();

        //    Respuesta respuestaTransaccion;

        //    try
        //    {
        //        EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();

        //        usuarioResponse = ePBL_Usuario.Editar(usuarioRequest);

        //        respuestaTransaccion = usuarioResponse.respuestaTransaccion;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        respuestaTransaccion = new Respuesta() { p_idRespuesta = -1, p_mensajeRespuesta = "Error - " + ex.Message };
        //    }

        //    return Json(JsonConvert.SerializeObject(respuestaTransaccion));
        //}

        //[HttpPost]
        //public JsonResult ListarUsuario(int idUsuario,int estado)
        //{
        //    UsuarioResponse usuarioResponse = new UsuarioResponse();
        //    string url = "https://localhost:44391/usuario/" + idUsuario.ToString();            
        //    WebRequest webRequest = WebRequest.Create(url);
        //    webRequest.Method = "get";
        //    webRequest.ContentType = "application/json;charset-UTF-8";
        //    WebResponse webResponse = webRequest.GetResponse();
        //    using (var service = new StreamReader(webResponse.GetResponseStream()))
        //    {
        //        usuarioResponse = JsonConvert.DeserializeObject<UsuarioResponse>(service.ReadToEnd().Trim());
        //    }
        //    return Json(JsonConvert.SerializeObject(usuarioResponse));
           
        //}

        //[HttpPost]
        //public JsonResult ListarUsuarioCombo(int tipoListado)
        //{
        //    UsuarioRequest usuarioRequest = new UsuarioRequest();
        //    usuarioRequest.tipoListado = tipoListado;

        //    UsuarioResponse usuarioResponse = new UsuarioResponse();

        //    Respuesta respuestaTransaccion;

        //    try
        //    {
        //        EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();

        //        usuarioResponse = ePBL_Usuario.ListarUsuarioCombo(usuarioRequest);

        //        respuestaTransaccion = usuarioResponse.respuestaTransaccion;
        //    }
        //    catch (Exception ex)
        //    {
        //        respuestaTransaccion = new Respuesta() { p_idRespuesta = -1, p_mensajeRespuesta = ex.Message };
        //    }

        //    usuarioResponse.respuestaTransaccion = respuestaTransaccion;

        //    return Json(JsonConvert.SerializeObject(usuarioResponse));
        //}
                
        //[HttpPost]
        //public JsonResult EliminarUsuario(int idUsuario)
        //{
        //    ENT_Usuario datosUsuario = new ENT_Usuario();
        //    datosUsuario.idUsuario = idUsuario;
        //    int idUsuaBaja = idUsuario; //sato será reemplazado por usuario de sesión
        //    datosUsuario.idUsuaBaja = idUsuaBaja;

        //    UsuarioRequest usuarioRequest = new UsuarioRequest();
        //    usuarioRequest.eNT_Usuario = datosUsuario;

        //    UsuarioResponse usuarioResponse = new UsuarioResponse();

        //    Respuesta respuestaTransaccion;

        //    try
        //    {
        //        EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();

        //        usuarioResponse = ePBL_Usuario.Eliminar(usuarioRequest);

        //        respuestaTransaccion = usuarioResponse.respuestaTransaccion;
        //    }
        //    catch (Exception ex)
        //    {

        //        Console.WriteLine(ex.Message);
        //        respuestaTransaccion = new Respuesta() { p_idRespuesta = -1, p_mensajeRespuesta = "Error - " + ex.Message };
        //    }

        //    return Json(JsonConvert.SerializeObject(respuestaTransaccion));
        //}       

        public JsonResult ConsultaDNI_RUC(string documento, int codSunat)
        {
            EntidadSUNAT entidadSUNAT = new EntidadSUNAT();
            EntidadRENIEC entidadRENIEC = new EntidadRENIEC();
            try
            {
                if (codSunat == 6)
                {
                    ConsultaRUC consultaRUC = new ConsultaRUC();

                    entidadSUNAT = consultaRUC.DatosSunat(documento);

                    return Json(JsonConvert.SerializeObject(entidadSUNAT));
                }
                else
                {
                    ConsultaDNI consultaDNI = new ConsultaDNI();

                    entidadRENIEC = consultaDNI.DatosReniec(documento);

                    return Json(JsonConvert.SerializeObject(entidadRENIEC));
                }
            }
            catch (Exception ex)
            {
                return Json(new { resultado = -1, mensaje = ex.Message });
            }

        }
        public string encriptarClave(string clavein)
        {
            string letras = "#$%&=<6789abcdefghijklmnABCDEGFHIJKLMN,ñopqrstuvwxyzÑOPQRSTUVWXYZ12345?;._@:-{}[]+-*/ 0¡|!>";

            string claveEncript = "";

            foreach (char letra in clavein)
            {
                int x = letras.IndexOf(letra);
                claveEncript += x.ToString() + "L";
            }
            claveEncript += "T";
            claveEncript = claveEncript.Replace("LT", string.Empty);

            return claveEncript;

        }
        public string desencriptarClave(string claveEncript)
        {
            string letras = "#$%&=<6789abcdefghijklmnABCDEGFHIJKLMN,ñopqrstuvwxyzÑOPQRSTUVWXYZ12345?;._@:-{}[]+-*/ 0¡|!>";
            string claveIn = "";

            try
            {
                foreach (string letra in claveEncript.Split('L').ToList())
                {
                    int x = int.Parse(letra);
                    claveIn += letras.ElementAt(x);
                }
            }
            catch (Exception)
            {
                claveIn = claveEncript;
            }

            return claveIn;

        }

    }
}