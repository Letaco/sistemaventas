﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaVentasLetacoSolution
{
    public static class Utilidades
    {
        public static byte[] ImagetoByteArray(Image image)
        {
            byte[] imageResponse;

            try
            {
                string sTemp = Path.GetTempFileName();
                FileStream fs = new FileStream(sTemp, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                image.Save(fs, System.Drawing.Imaging.ImageFormat.Png);
                fs.Position = 0;
                int imgLength = Convert.ToInt32(fs.Length);
                imageResponse = new byte[imgLength];
                fs.Read(imageResponse, 0, imgLength);
                fs.Close();
            }
            catch (Exception ex)
            {
                imageResponse = null;
            }

            return imageResponse;
        }
    }
}
