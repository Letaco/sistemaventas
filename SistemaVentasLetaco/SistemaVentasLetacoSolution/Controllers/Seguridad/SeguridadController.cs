﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SistemaVentasLetacoSolution.Controllers.Seguridad
{
    public class SeguridadController : Controller
    {
        // GET: Seguridad
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Objeto()
        {
            return View();
        }

        public ActionResult Pantalla()
        {
            return View();
        }

        public ActionResult Roles()
        {
            return View();
        }

        // GET: Seguridad/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Seguridad/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Seguridad/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Seguridad/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Seguridad/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Seguridad/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Seguridad/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}