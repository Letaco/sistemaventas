﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Entidades;
using EnlacePresentacion;
using EntidadesIntermedias;
using Newtonsoft.Json;

namespace SistemaVentasLetacoSolution.Controllers
{
    public class ConfiguracionController : Controller
    {
        // GET: Configuracion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TablaSUNAT()
        {
            return View();
        }

        public ActionResult GestionBancos()
        {
            return View();
        }

        public ActionResult Almacen()
        {
            return View();
        }

        public ActionResult Otros()
        {
            return View();
        }

        // GET: Configuracion/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Configuracion/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Configuracion/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Configuracion/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Configuracion/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Configuracion/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Configuracion/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public JsonResult ListarTipoPersonaCombo()
        {
            Respuesta respuestaTransaccion;

            TipoPersonaRequest tipoPersonaRequest = new TipoPersonaRequest();

            TipoPersonaResponse tipoPersonaResponse = new TipoPersonaResponse();            

            try
            {
                EPBL_TipoPersona ePBL_TipoPersona = new EPBL_TipoPersona();

                tipoPersonaResponse = ePBL_TipoPersona.ListarTipoPersonaCombo(tipoPersonaRequest);

                respuestaTransaccion = tipoPersonaResponse.respuestaTransaccion;
            }
            catch (Exception ex)
            {
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            tipoPersonaResponse.respuestaTransaccion = respuestaTransaccion;

            return Json(JsonConvert.SerializeObject(tipoPersonaResponse));
        }

        public JsonResult ListarTipoDocIdentidadCombo(int idTipoPersona)
        {
            Respuesta respuestaTransaccion;

            TipoDocIdentidadRequest tipoDocIdentidadRequest = new TipoDocIdentidadRequest();
            tipoDocIdentidadRequest.eNT_TipoDocIdentidad = new ENT_TipoDocIdentidad() { idTipoPersona = idTipoPersona };

            TipoDocIdentidadResponse tipoDocIdentidadResponse = new TipoDocIdentidadResponse();

            try
            {
                EPBL_TipoDocIdentidad ePBL_TipoDocIdentidad = new EPBL_TipoDocIdentidad();

                tipoDocIdentidadResponse = ePBL_TipoDocIdentidad.ListarTipoDocIdentidadCombo(tipoDocIdentidadRequest);

                respuestaTransaccion = tipoDocIdentidadResponse.respuestaTransaccion;
            }
            catch (Exception ex)
            {
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            tipoDocIdentidadResponse.respuestaTransaccion = respuestaTransaccion;

            return Json(JsonConvert.SerializeObject(tipoDocIdentidadResponse));
        }
    }
}