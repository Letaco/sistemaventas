﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SistemaVentasLetacoSolution.Controllers.Logistica
{
    public class LogisticaController : Controller
    {
        // GET: Logistica
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SeccionAlmacen()
        {
            return View();
        }

        public ActionResult MarcaModelo()
        {
            return View();
        }

        public ActionResult FamiliaProductos()
        {
            return View();
        }

        public ActionResult Producto()
        {
            return View();
        }

        public ActionResult ClasificacionCliente()
        {
            return View();
        }

        public ActionResult Cliente()
        {
            return View();
        }

        public ActionResult PrecioProducto()
        {
            return View();
        }

        public ActionResult MovimientoAlmacen()
        {
            return View();
        }

        public ActionResult AprobacionMovimiento()
        {
            return View();
        }

        // GET: Logistica/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Logistica/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Logistica/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Logistica/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Logistica/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Logistica/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Logistica/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}