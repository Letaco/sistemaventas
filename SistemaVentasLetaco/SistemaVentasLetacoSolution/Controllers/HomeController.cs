﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SistemaVentasLetacoSolution.Models;
using SistemaVentasLetacoSolution.ViewModels.Sistema.Seguridad;
using System.Configuration;
using EntidadesIntermedias;
using EnlacePresentacion;
using Entidades;

namespace SistemaVentasLetacoSolution.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        SessionPermisoViewModel lsv;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            /* Descomentar para probar funcionalidad
             * nota: Si la tabla está vacía debe responder la entiadd con 0 filas
             
            // inicio prueba de llamada a la bd
            EPBL_TablaSunat ePBL_TablaSunat = new EPBL_TablaSunat();
            TablaSunatRequest tablaSunatRequest = new TablaSunatRequest();
            tablaSunatRequest.estado = "";
            tablaSunatRequest.nombreTabla = "";
            tablaSunatRequest.id = 0;

            TablaSunatResponse tablaSunatResponse = ePBL_TablaSunat.ListaTablaSunat(tablaSunatRequest);

            if (tablaSunatResponse != null)
            {
                if (tablaSunatResponse.tablaSunatList != null)
                {
                    List<ENT_TablaSunat> listaTablaSunat = tablaSunatResponse.tablaSunatList;
                }
            }
            // fin prueba de llamada a la bd

            */

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public ActionResult Menu()
        {
            return View();
        }

    }
}
