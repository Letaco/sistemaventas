﻿$(function () {
    
    $("#LimpiarInternobtn").on("click", function () {        
        $("#interno").val("");
        $("#documento").val("");
        $("#apellidos").val("");
        $("#nombres").val("");
        $('#tableInternos').DataTable().clear().destroy();
        $("#popupErrorBuscarInterno2").css('display', 'none');
     });

    var table = {}
    $("#doc_num_vis").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $("#btnBuscarReniec").on("click", function () {
        dtCargarDatos();
    });
    $('#modalBusquedaReniec').on('hidden.bs.modal', function (e) {
        $(this).find("input,textarea,select").val('').end().find("input[type=checkbox], input[type=radio]").prop("checked", "").end();
        $("#dataTableMvc tbody tr").remove();
        $("#dataTableMvc_info").html("");
        $("#dataTableMvc_paginate").html("");
    });   

    function dtCargarDatos() {

        var dniVisitante = $("#idNumDocumentoReniec").val();
        var apePaterVisitante = $("#idApePaterno").val();
        var apaMaterVisitante = $("#idApeMaterno").val();
        var nombresVisitante = $("#idNombres").val();

        var error = 0;
        var sms = "";

        if (apePaterVisitante == null || parseInt(apePaterVisitante) < 0 || (apePaterVisitante == "")) {
            error = 1;
            sms = sms + " <li>Ingresar Apellido Paterno</li>";
        }
        if (apaMaterVisitante == null || parseInt(apaMaterVisitante) < 0 || (apaMaterVisitante == "")) {
            error = 1;
            sms = sms + " <li>Ingresar Apellido Materno</li>";
        }
        if (nombresVisitante == null || parseInt(nombresVisitante) < 0 || (nombresVisitante == "")) {
            error = 1;
            sms = sms + " <li>Ingresar Nombres</li>";
        }

        if (error != 1) {
            var parametros = {
                tipoConsulta: "0",
                dni: $("#idNumDocumentoReniec").val(),
                apePaterno: $("#idApePaterno").val(),
                apeMaterno: $("#idApeMaterno").val(),
                nombres: $("#idNombres").val()
            }

            table = $("#dataTableMvc").DataTable({
                destroy: true,
                responsive: true,
                "pageLength": 3,
                ajax: {
                    method: "POST",
                    url: "http://localhost:39327/Visitante/BuscarEnReniec",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: function (d) {
                        d.parametros = parametros;
                        return JSON.stringify(d);
                    },
                    dataSrc: function (d) {
                        return d.data;
                    }
                },

                "language":
                {
                    "processing": "Refrescando data...",
                    "lengthMenu": "items x pág. _MENU_",
                    "zeroRecords": "No se encontraron registros.",
                    "info": "Mostrando del _START_ al _END_ de _TOTAL_ registros en total",
                    "infoEmpty": "No hay registros.",
                    "infoFiltered": "",
                    "search": "",
                    "paginate": {
                        "first": "<span class=\"icon fa fa-angle-double-left\"></span>",
                        "previous": "<span class=\"icon fa fa-angle-left\"></span>",
                        "next": "<span class=\"icon fa fa-angle-right\"></span>",
                        "last": "<span class=\"icon fa fa-angle-double-right\"></span>"
                    }
                },
                "sDom": ' t <"padding-xs-hr" <"table-caption no-padding"> <"clear"T> <"DT-lf-right"> > t <"table-footer padding-xs-hr clearfix" <"DT-label"i> <"DT-pagination"p> >',
                "oTableTools": {
                    "aButtons": [
                    ],
                    "sSwfPath": "${pageContext.request.contextPath}/assets/libs/DataTables-1.10.2/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
                },
                columns: [
                    { "data": "NumeroDNI" },
                    { "data": "ApellidoPaterno" },
                    { "data": "ApellidoMaterno" },
                    { "data": "PreNombres" },
                    {
                        "defaultContent": "<button type='button' id='verDetallesReniec' name='verDetallesReniec' class='btn btn-info'>Ver Detalle</button>"
                    },
                    {
                        "defaultContent": "<button type='button'  class='btn btn-danger'>Recuperar</button>"
                    }
                ]
            });

            $('#dataTableMvc tbody').on('click', 'button.btn.btn-danger', function () {                
                var data = table.row($(this).parents('tr')).data();
                var parametros = {
                    vis_nom: data.PreNombres,
                    vis_ape_pat: data.ApellidoPaterno,
                    vis_ape_mat: data.ApellidoMaterno,
                    sex_id: 1,
                    doc_num_vis: data.NumeroDNI,
                    int_id_ref: $("#int_id_ref").val(),
                    valReniec : true
                }
                data.creacion = parametros;
                var url = 'http://localhost:39327/Visitante/CreateModel/';
                url += "?dni=" + parametros.doc_num_vis;
                url += "&paterno=" + parametros.vis_ape_pat;
                url += "&materno=" + parametros.vis_ape_mat;
                url += "&nombre=" + parametros.vis_nom;
                url += "&id_ref=" + parametros.int_id_ref;
                url += "&sexo=" + parametros.sex_id;
                url += "&valReniec=" + parametros.valReniec;

                
                window.location.href = url;
            });

            $('#dataTableMvc tbody').on('click', 'button.btn.btn-info', function () {
                var data = table.row($(this).parents('tr')).data()
                var parametros = {
                    tipoConsulta: "0",
                    dni: data.NumeroDNI,
                    apePaterno: "",
                    apeMaterno: "",
                    nombres: ""
                }
                data.parametros = parametros;
                $.ajax({
                    method: "POST",
                    url: "http://localhost:39327/Visitante/verDetalleReniec",
                    contentType: "application/json; charset=utf-8",
                    dataType: "html",
                    data: JSON.stringify(data),
                    success: function (response) {
                        console.log(response);
                        $("#partialModal").html(response);
                        $("#modalPartial").modal('show');
                    }
                });
            });

        } else if (error == 1) {
            $("#txtPopupErrorRegistrarVisitaDatos").html("<ul type='circle' style='padding-left: 13px;'>" + sms + "</ul>");
            $("#popupErrorRegistrarVisitaDatos").css("display", "block");
            return false;
        }
    }
})

function cargarDatosReniec() {
    var dniVisitante = $("#doc_num_vis").val();
    var error = 0;
    var sms = "";
    if (dniVisitante == null || parseInt(dniVisitante) < 0 || (dniVisitante == "")) {
        error = 1;
        sms = sms + " <li>Ingresar DNI</li>";
    }
    if (error != 1) {
        $.ajax({
            type: "POST",
            url: "/Sinceramiento/BuscarReniecPorDni",
            data: { 'dni': $("#doc_num_vis").val() },           
            success: function (result) {
                $("#vis_ape_pat").val(result.ApellidoPaterno);
                $("#vis_ape_mat").val(result.ApellidoMaterno);
                $("#vis_nom").val(result.Nombres);
                if (result.Sexo == 1) { // Masculino                    
                    $("#sex_id").val("1");
                } else {                   
                    $("#sex_id").val("2");
                }                
                if (result.PrefijoBlockChalet == null || result.PrefijoBlockChalet == "") {
                    $("#imgVisitante").attr("src", "/Content/img/nohay_foto_small.png");
                } else {                    
                    $("#imgVisitante").attr("src", "data:image/png;base64," + result.PrefijoBlockChalet);
                    $("#PrefijoBlockChalet").val(result.PrefijoBlockChalet);
                }
            }
        });     
    }
    else if (error == 1) {
        $("#txtPopupErrorCulminarRegistroVisitas").html("<ul type='circle' style='padding-left: 13px;'>" + sms + "</ul>");
        $("#popupErrorCulminarRegistroVisitas").css("display", "block");
        return false;
    }
    return false;
};  
$("#cargarDatosReniec").click(cargarDatosReniec);
function verDetallesReniec() {
    var dniVisitante = $("#idNumDocumentoReniec").val();
    var error = 0;
    var sms = "";
    if (dniVisitante == null || parseInt(dniVisitante) < 0 || (dniVisitante == "")) {
        error = 1;
        sms = sms + " <li>Ingresar DNI</li>";
    }

    if (error != 1) {
        var data = {};
        var parametros = {
            tipoConsulta: "0",
            dni: $("#idNumDocumentoReniec").val(),
            apePaterno: "",
            apeMaterno: "",
            nombres: ""
        }
        data.parametros = parametros;
        $.ajax({
            method: "POST",
            url: "/Sinceramiento/verDetalleReniec",
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            data: JSON.stringify(data),
            success: function (response) {
                console.log(response);
                $("#partialModal").html(response);
                $("#modalPartial").modal('show');
            }
        });
    }
    else if (error == 1) {
        $("#txtPopupErrorRegistrarVisitaDatos").html("<ul type='circle' style='padding-left: 13px;'>" + sms + "</ul>");
        $("#popupErrorRegistrarVisitaDatos").css("display", "block");
        return false;
    }
    
};

$("#verDetallesReniec").click(verDetallesReniec);


function ponleFocus2() {
    document.getElementById("doc_num_vis").focus();
}