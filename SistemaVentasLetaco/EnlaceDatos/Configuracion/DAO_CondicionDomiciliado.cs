﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Entidades;
using MySql.Data.MySqlClient;

namespace EnlaceDatos
{
    [Serializable]
    public class DAO_CondicionDomiciliado
    {
        Conexion conexion;

        public DAO_CondicionDomiciliado()
        {
            conexion = new Conexion();
        }

        public Respuesta Insertar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();
            respuesta.idRespuesta = 0;
            respuesta.mensajeRespuesta = string.Empty;            

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.CONF_Insertar_CondicionDomiciliado, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_CondicionDomiciliado eNT_CondicionDomiciliado = (ENT_CondicionDomiciliado)valoresPermitidos[0];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_codSunat, MySqlDbType.VarChar);
                mySqlParameter1.Value = eNT_CondicionDomiciliado.codSunat;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter2 = new MySqlParameter(ListAtributos.p_descripcion, MySqlDbType.VarChar);
                mySqlParameter2.Value = eNT_CondicionDomiciliado.descripcion;
                mySqlParameter2.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter3 = new MySqlParameter(ListAtributos.p_descripcionCorta, MySqlDbType.VarChar);
                mySqlParameter3.Value = eNT_CondicionDomiciliado.descripcionCorta;
                mySqlParameter3.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter4 = new MySqlParameter(ListAtributos.p_estado, MySqlDbType.VarChar);
                mySqlParameter4.Value = eNT_CondicionDomiciliado.estado;
                mySqlParameter4.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter5 = new MySqlParameter(ListAtributos.p_idUsuaReg, MySqlDbType.VarChar);
                mySqlParameter5.Value = eNT_CondicionDomiciliado.idUsuaReg;
                mySqlParameter5.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter6 = new MySqlParameter(ListAtributos.p_fechaReg, MySqlDbType.VarChar);
                mySqlParameter6.Value = eNT_CondicionDomiciliado.fechaReg;
                mySqlParameter6.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameterSalida = new MySqlParameter(ListAtributos.p_id, MySqlDbType.Int32);
                mySqlParameterSalida.Direction = ParameterDirection.Output;

                mySqlCommand.Parameters.Add(mySqlParameter1);
                mySqlCommand.Parameters.Add(mySqlParameter2);
                mySqlCommand.Parameters.Add(mySqlParameter3);
                mySqlCommand.Parameters.Add(mySqlParameter4);
                mySqlCommand.Parameters.Add(mySqlParameter5);
                mySqlCommand.Parameters.Add(mySqlParameter6);
                mySqlCommand.Parameters.Add(mySqlParameterSalida);

                mySqlCommand.ExecuteNonQuery();

                respuesta.idRespuesta = int.Parse(mySqlCommand.Parameters[ListAtributos.p_id].Value.ToString());
                respuesta.mensajeRespuesta = ListAtributos.msj_respuestaCorrectaInsert;
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            finally {
                mySqlConnection.Close();
            }

            return respuesta;

        }

        public Respuesta Actualizar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();
            respuesta.idRespuesta = 0;
            respuesta.mensajeRespuesta = string.Empty;

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.CONF_Editar_CondicionDomiciliado, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_CondicionDomiciliado eNT_CondicionDomiciliado = (ENT_CondicionDomiciliado)valoresPermitidos[0];

                MySqlParameter mySqlParameter0 = new MySqlParameter(ListAtributos.p_idCondicionDomiciliado, MySqlDbType.VarChar);
                mySqlParameter0.Value = eNT_CondicionDomiciliado.idCondicionDomiciliado;
                mySqlParameter0.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_codSunat, MySqlDbType.VarChar);
                mySqlParameter1.Value = eNT_CondicionDomiciliado.codSunat;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter2 = new MySqlParameter(ListAtributos.p_descripcion, MySqlDbType.VarChar);
                mySqlParameter2.Value = eNT_CondicionDomiciliado.descripcion;
                mySqlParameter2.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter3 = new MySqlParameter(ListAtributos.p_descripcionCorta, MySqlDbType.VarChar);
                mySqlParameter3.Value = eNT_CondicionDomiciliado.descripcionCorta;
                mySqlParameter3.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter4 = new MySqlParameter(ListAtributos.p_estado, MySqlDbType.VarChar);
                mySqlParameter4.Value = eNT_CondicionDomiciliado.estado;
                mySqlParameter4.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter5 = new MySqlParameter(ListAtributos.p_idUsuActv, MySqlDbType.VarChar);
                mySqlParameter5.Value = eNT_CondicionDomiciliado.idUsuActv;
                mySqlParameter5.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter6 = new MySqlParameter(ListAtributos.p_fechaActv, MySqlDbType.VarChar);
                mySqlParameter6.Value = eNT_CondicionDomiciliado.fechaActv;
                mySqlParameter6.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter7 = new MySqlParameter(ListAtributos.p_idUsuaBaja, MySqlDbType.VarChar);
                mySqlParameter5.Value = eNT_CondicionDomiciliado.idUsuaBaja;
                mySqlParameter5.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter8 = new MySqlParameter(ListAtributos.p_fechabaja, MySqlDbType.VarChar);
                mySqlParameter6.Value = eNT_CondicionDomiciliado.fechabaja;
                mySqlParameter6.Direction = ParameterDirection.Input;

                mySqlCommand.Parameters.Add(mySqlParameter1);
                mySqlCommand.Parameters.Add(mySqlParameter2);
                mySqlCommand.Parameters.Add(mySqlParameter3);
                mySqlCommand.Parameters.Add(mySqlParameter4);
                mySqlCommand.Parameters.Add(mySqlParameter5);
                mySqlCommand.Parameters.Add(mySqlParameter6);
                mySqlCommand.Parameters.Add(mySqlParameter7);
                mySqlCommand.Parameters.Add(mySqlParameter8);

                mySqlCommand.ExecuteNonQuery();

                respuesta.idRespuesta = 1;
                respuesta.mensajeRespuesta = ListAtributos.msj_respuestaCorrectaEditar;
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            finally {
                mySqlConnection.Close();
            }

            return respuesta;

        }

        public List<ENT_CondicionDomiciliado> Listar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();
            respuesta.idRespuesta = 0;
            respuesta.mensajeRespuesta = string.Empty;

            List<ENT_CondicionDomiciliado> eNT_CondicionDomiciliadoListResponse = new List<ENT_CondicionDomiciliado>();
            
            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.CONF_Listar_CondicionDomiciliado, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_CondicionDomiciliado eNT_CondicionDomiciliado =  (ENT_CondicionDomiciliado)valoresPermitidos[0];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_codSunat, MySqlDbType.VarChar);
                mySqlParameter1.Value = eNT_CondicionDomiciliado.codSunat;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
                DataTable _dataTable = new DataTable();
                mySqlDataAdapter.Fill(_dataTable);

                if (_dataTable != null)
                {
                    foreach (DataRow item in _dataTable.Rows)
                    {
                        ENT_CondicionDomiciliado eNT_CondicionDomiciliadoEX = new ENT_CondicionDomiciliado();

                        eNT_CondicionDomiciliadoEX.idCondicionDomiciliado = int.Parse(item[0].ToString());
                        eNT_CondicionDomiciliadoEX.codSunat = item[1].ToString();
                        eNT_CondicionDomiciliadoEX.descripcion = item[2].ToString();
                        eNT_CondicionDomiciliadoEX.descripcionCorta = item[3].ToString();
                        eNT_CondicionDomiciliadoEX.descripcionEstado = item[4].ToString();
                        eNT_CondicionDomiciliadoEX.estado = int.Parse(item[5].ToString());
                        eNT_CondicionDomiciliadoEX.idUsuaReg = int.Parse(item[6].ToString());
                        eNT_CondicionDomiciliadoEX.fechaReg = DateTime.Parse(item[7].ToString());
                        eNT_CondicionDomiciliadoEX.nombreUsuarioReg = item[8].ToString();
                        eNT_CondicionDomiciliadoEX.idUsuaBaja = int.Parse(item[9].ToString());
                        eNT_CondicionDomiciliadoEX.fechabaja = DateTime.Parse(item[10].ToString());
                        eNT_CondicionDomiciliadoEX.nombreUsuarioBaja = item[11].ToString();
                        eNT_CondicionDomiciliadoEX.idUsuActv = int.Parse(item[12].ToString());
                        eNT_CondicionDomiciliadoEX.fechaActv = DateTime.Parse(item[13].ToString());
                        eNT_CondicionDomiciliadoEX.nombreUsuarioActv = item[14].ToString();

                        eNT_CondicionDomiciliadoListResponse.Add(eNT_CondicionDomiciliadoEX);
                    }
                }
            }
            catch (Exception ex)
            {
                ENT_CondicionDomiciliado eNT_CondicionDomiciliadoEX = new ENT_CondicionDomiciliado();
                eNT_CondicionDomiciliadoEX.Respuesta.mensajeRespuesta = ex.Message;
                eNT_CondicionDomiciliadoEX.Respuesta.idRespuesta = -1;

                eNT_CondicionDomiciliadoListResponse.Add(eNT_CondicionDomiciliadoEX);
            }
            finally {
                mySqlConnection.Close();
            }

            return eNT_CondicionDomiciliadoListResponse;
        }
    }
}
