﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Entidades;
using MySql.Data.MySqlClient;

namespace EnlaceDatos
{
    [Serializable]
    public class DAO_NivelEducativo
    {
        Conexion conexion;

        public DAO_NivelEducativo()
        {
            conexion = new Conexion();
        }

        public Respuesta Insertar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();
            respuesta.idRespuesta = 0;
            respuesta.mensajeRespuesta = string.Empty;            

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.CONF_Insertar_NivelEducativo, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_NivelEducativo eNT_NivelEducativo = (ENT_NivelEducativo)valoresPermitidos[0];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_codSunat, MySqlDbType.VarChar);
                mySqlParameter1.Value = eNT_NivelEducativo.codSunat;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter2 = new MySqlParameter(ListAtributos.p_descripcion, MySqlDbType.VarChar);
                mySqlParameter2.Value = eNT_NivelEducativo.descripcion;
                mySqlParameter2.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter3 = new MySqlParameter(ListAtributos.p_descripcionCorta, MySqlDbType.VarChar);
                mySqlParameter3.Value = eNT_NivelEducativo.descripcionCorta;
                mySqlParameter3.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter4 = new MySqlParameter(ListAtributos.p_estado, MySqlDbType.VarChar);
                mySqlParameter4.Value = eNT_NivelEducativo.estado;
                mySqlParameter4.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter5 = new MySqlParameter(ListAtributos.p_idUsuaReg, MySqlDbType.VarChar);
                mySqlParameter5.Value = eNT_NivelEducativo.idUsuaReg;
                mySqlParameter5.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter6 = new MySqlParameter(ListAtributos.p_fechaReg, MySqlDbType.VarChar);
                mySqlParameter6.Value = eNT_NivelEducativo.fechaReg;
                mySqlParameter6.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameterSalida = new MySqlParameter(ListAtributos.p_id, MySqlDbType.Int32);
                mySqlParameterSalida.Direction = ParameterDirection.Output;

                mySqlCommand.Parameters.Add(mySqlParameter1);
                mySqlCommand.Parameters.Add(mySqlParameter2);
                mySqlCommand.Parameters.Add(mySqlParameter3);
                mySqlCommand.Parameters.Add(mySqlParameter4);
                mySqlCommand.Parameters.Add(mySqlParameter5);
                mySqlCommand.Parameters.Add(mySqlParameter6);
                mySqlCommand.Parameters.Add(mySqlParameterSalida);

                mySqlCommand.ExecuteNonQuery();

                respuesta.idRespuesta = int.Parse(mySqlCommand.Parameters[ListAtributos.p_id].Value.ToString());
                respuesta.mensajeRespuesta = ListAtributos.msj_respuestaCorrectaInsert;
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }

            return respuesta;

        }

        public Respuesta Actualizar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();
            respuesta.idRespuesta = 0;
            respuesta.mensajeRespuesta = string.Empty;

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.CONF_Editar_NivelEducativo, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_NivelEducativo eNT_NivelEducativo = (ENT_NivelEducativo)valoresPermitidos[0];

                MySqlParameter mySqlParameter0 = new MySqlParameter(ListAtributos.p_idNivelEducativo, MySqlDbType.VarChar);
                mySqlParameter0.Value = eNT_NivelEducativo.idNivelEducativo;
                mySqlParameter0.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_codSunat, MySqlDbType.VarChar);
                mySqlParameter1.Value = eNT_NivelEducativo.codSunat;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter2 = new MySqlParameter(ListAtributos.p_descripcion, MySqlDbType.VarChar);
                mySqlParameter2.Value = eNT_NivelEducativo.descripcion;
                mySqlParameter2.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter3 = new MySqlParameter(ListAtributos.p_descripcionCorta, MySqlDbType.VarChar);
                mySqlParameter3.Value = eNT_NivelEducativo.descripcionCorta;
                mySqlParameter3.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter4 = new MySqlParameter(ListAtributos.p_estado, MySqlDbType.VarChar);
                mySqlParameter4.Value = eNT_NivelEducativo.estado;
                mySqlParameter4.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter5 = new MySqlParameter(ListAtributos.p_idUsuActv, MySqlDbType.VarChar);
                mySqlParameter5.Value = eNT_NivelEducativo.idUsuActv;
                mySqlParameter5.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter6 = new MySqlParameter(ListAtributos.p_fechaActv, MySqlDbType.VarChar);
                mySqlParameter6.Value = eNT_NivelEducativo.fechaActv;
                mySqlParameter6.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter7 = new MySqlParameter(ListAtributos.p_idUsuaBaja, MySqlDbType.VarChar);
                mySqlParameter5.Value = eNT_NivelEducativo.idUsuaBaja;
                mySqlParameter5.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter8 = new MySqlParameter(ListAtributos.p_fechabaja, MySqlDbType.VarChar);
                mySqlParameter6.Value = eNT_NivelEducativo.fechabaja;
                mySqlParameter6.Direction = ParameterDirection.Input;

                mySqlCommand.Parameters.Add(mySqlParameter1);
                mySqlCommand.Parameters.Add(mySqlParameter2);
                mySqlCommand.Parameters.Add(mySqlParameter3);
                mySqlCommand.Parameters.Add(mySqlParameter4);
                mySqlCommand.Parameters.Add(mySqlParameter5);
                mySqlCommand.Parameters.Add(mySqlParameter6);
                mySqlCommand.Parameters.Add(mySqlParameter7);
                mySqlCommand.Parameters.Add(mySqlParameter8);

                mySqlCommand.ExecuteNonQuery();

                respuesta.idRespuesta = 1;
                respuesta.mensajeRespuesta = ListAtributos.msj_respuestaCorrectaEditar;
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }

            return respuesta;

        }

        public List<ENT_NivelEducativo> Listar(params object[] valoresPermitidos)
        {
            List<ENT_NivelEducativo> eNT_NivelEducativoListResponse = new List<ENT_NivelEducativo>();

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.CONF_Listar_NivelEducativo, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_NivelEducativo eNT_NivelEducativo = (ENT_NivelEducativo)valoresPermitidos[0];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_codSunat, MySqlDbType.VarChar);
                mySqlParameter1.Value = eNT_NivelEducativo.codSunat;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
                DataTable _dataTable = new DataTable();
                mySqlDataAdapter.Fill(_dataTable);

                if (_dataTable != null)
                {
                    foreach (DataRow item in _dataTable.Rows)
                    {
                        ENT_NivelEducativo eNT_NivelEducativoEX = new ENT_NivelEducativo();

                        eNT_NivelEducativoEX.idNivelEducativo = int.Parse(item[0].ToString());
                        eNT_NivelEducativoEX.codSunat = item[1].ToString();
                        eNT_NivelEducativoEX.descripcion = item[2].ToString();
                        eNT_NivelEducativoEX.descripcionCorta = item[3].ToString();
                        eNT_NivelEducativoEX.descripcionEstado = item[4].ToString();
                        eNT_NivelEducativoEX.estado = int.Parse(item[5].ToString());
                        eNT_NivelEducativoEX.idUsuaReg = int.Parse(item[6].ToString());
                        eNT_NivelEducativoEX.fechaReg = DateTime.Parse(item[7].ToString());
                        eNT_NivelEducativoEX.nombreUsuarioReg = item[8].ToString();
                        eNT_NivelEducativoEX.idUsuaBaja = int.Parse(item[9].ToString());
                        eNT_NivelEducativoEX.fechabaja = DateTime.Parse(item[10].ToString());
                        eNT_NivelEducativoEX.nombreUsuarioBaja = item[11].ToString();
                        eNT_NivelEducativoEX.idUsuActv = int.Parse(item[12].ToString());
                        eNT_NivelEducativoEX.fechaActv = DateTime.Parse(item[13].ToString());
                        eNT_NivelEducativoEX.nombreUsuarioActv = item[14].ToString();

                        eNT_NivelEducativoListResponse.Add(eNT_NivelEducativoEX);
                    }
                }
            }
            catch (Exception ex)
            {
                ENT_NivelEducativo eNT_NivelEducativoEX = new ENT_NivelEducativo();
                eNT_NivelEducativoEX.Respuesta.mensajeRespuesta = ex.Message;
                eNT_NivelEducativoEX.Respuesta.idRespuesta = -1;

                eNT_NivelEducativoListResponse.Add(eNT_NivelEducativoEX);
            }
            finally
            {
                mySqlConnection.Close();
            }

            return eNT_NivelEducativoListResponse;
        }
    }
}
