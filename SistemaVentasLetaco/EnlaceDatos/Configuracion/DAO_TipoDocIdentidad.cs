﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Entidades;
using MySql.Data.MySqlClient;

namespace EnlaceDatos
{
    [Serializable]
    public class DAO_TipoDocIdentidad
    {
        Conexion conexion;

        public DAO_TipoDocIdentidad()
        {
            conexion = new Conexion();
        }

        public List<ENT_TipoDocIdentidad> ListarTipoDocIdentidad(params object[] valoresPermitidos)
        {
            List<ENT_TipoDocIdentidad> eNT_TipoDocIdentidadListResponse = new List<ENT_TipoDocIdentidad>();
            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.CONF_Listar_TipoDocIdentidad_Combo, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_TipoDocIdentidad eNT_TipoDocIdentidad = (ENT_TipoDocIdentidad)valoresPermitidos[0];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_idTipoPersona, MySqlDbType.Int32);
                mySqlParameter1.Value = eNT_TipoDocIdentidad.idTipoPersona;
                mySqlParameter1.Direction = ParameterDirection.Input;
                mySqlCommand.Parameters.Add(mySqlParameter1);

                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
                DataTable _dataTable = new DataTable();
                mySqlDataAdapter.Fill(_dataTable);

                if (_dataTable != null)
                {
                    foreach (DataRow item in _dataTable.Rows)
                    {
                        ENT_TipoDocIdentidad eNT_TipoDocIdentidadEX = new ENT_TipoDocIdentidad();

                        eNT_TipoDocIdentidadEX.idTipoDocIdentidad = int.Parse(item[0].ToString());
                        eNT_TipoDocIdentidadEX.idTipoPersona = int.Parse(item[1].ToString());
                        eNT_TipoDocIdentidadEX.codSunat = item[2].ToString();
                        eNT_TipoDocIdentidadEX.descripcion = item[3].ToString();
                        eNT_TipoDocIdentidadEX.descripcionCorta = item[4].ToString();
                        eNT_TipoDocIdentidadEX.cifrasValidacion = int.Parse(item[5].ToString());
                        eNT_TipoDocIdentidadEX.digitosExactos = int.Parse(item[6].ToString());
                        eNT_TipoDocIdentidadEX.Respuesta = new Respuesta() { idRespuesta = int.Parse(item[0].ToString()), mensajeRespuesta = ListAtributos.msj_respuestaCorrectaListar };
                        eNT_TipoDocIdentidadListResponse.Add(eNT_TipoDocIdentidadEX);
                    }
                }

            }
            catch (Exception ex)
            {
                ENT_TipoDocIdentidad eNT_TipoDocIdentidadEX = new ENT_TipoDocIdentidad();
                eNT_TipoDocIdentidadEX.Respuesta.mensajeRespuesta = ex.Message;
                eNT_TipoDocIdentidadEX.Respuesta.idRespuesta = -1;

                eNT_TipoDocIdentidadListResponse.Add(eNT_TipoDocIdentidadEX);
            }
            finally
            {
                mySqlConnection.Close();
            }
            return eNT_TipoDocIdentidadListResponse;

        }
    }
}
