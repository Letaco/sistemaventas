﻿using System;
using MySql.Data.MySqlClient;

namespace EnlaceDatos
{
    public class Conexion
    {
        private string cadena = Properties.Resources.BDDesaLocal;
        //private string cadena = Properties.Resources.BDTest;
        private MySqlConnection conexion;

        public Conexion() {
            if (conexion == null)
            {
                conexion = new MySqlConnection();
                conexion.ConnectionString = cadena;
            }
        }

        public MySqlConnection getConexion() {
            return this.conexion;
        }

        public string probarConexion()
        {
            try
            {
                this.conexion.Open();
                this.conexion.Close();
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
