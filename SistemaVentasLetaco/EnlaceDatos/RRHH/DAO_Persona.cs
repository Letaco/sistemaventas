﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Entidades;
using MySql.Data.MySqlClient;

namespace EnlaceDatos
{
    [Serializable]
    public class DAO_Persona
    {
        Conexion conexion;

        public DAO_Persona()
        {
            conexion = new Conexion();
        }

        public List<ENT_Persona> ListarPersonaCombo(params object[] valoresPermitidos)
        {

            List<ENT_Persona> eNT_PersonaListResponse = new List<ENT_Persona>();

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.RRHH_Listar_Persona_Combo, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_Persona eNT_Persona = (ENT_Persona)valoresPermitidos[0];
                int tipoListado = (int)valoresPermitidos[1];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_tipoListado, MySqlDbType.Int32);
                mySqlParameter1.Value = tipoListado;
                mySqlParameter1.Direction = ParameterDirection.Input;

                mySqlCommand.Parameters.Add(mySqlParameter1);

                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
                DataTable _dataTable = new DataTable();
                mySqlDataAdapter.Fill(_dataTable);

                if (_dataTable != null)
                {
                    foreach (DataRow item in _dataTable.Rows)
                    {
                        ENT_Persona eNT_PersonaEX = new ENT_Persona();

                        eNT_PersonaEX.idPersona = int.Parse(item[0].ToString());
                        eNT_PersonaEX.nombreCompleto = item[1].ToString();
                        eNT_PersonaEX.Respuesta = new Respuesta() { idRespuesta = int.Parse(item[0].ToString()), mensajeRespuesta = ListAtributos.msj_respuestaCorrectaListar };
                        eNT_PersonaListResponse.Add(eNT_PersonaEX);
                    }
                }

            }
            catch (Exception ex)
            {
                ENT_Persona eNT_PersonaEX = new ENT_Persona();
                eNT_PersonaEX.Respuesta = new Respuesta() { mensajeRespuesta = ex.Message, idRespuesta = -1 };

                eNT_PersonaListResponse.Add(eNT_PersonaEX);
            }
            finally
            {
                mySqlConnection.Close();
            }

            return eNT_PersonaListResponse;
        }

        public Respuesta Insertar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();
            respuesta.idRespuesta = 0;
            respuesta.mensajeRespuesta = string.Empty;

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.RRHH_Insertar_Persona, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_Persona eNT_Persona = (ENT_Persona)valoresPermitidos[0];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_idTipoPersona, MySqlDbType.Int32);
                mySqlParameter1.Value = eNT_Persona.idTipoPersona;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter2 = new MySqlParameter(ListAtributos.p_nombres_razSoc, MySqlDbType.VarChar);
                mySqlParameter2.Value = eNT_Persona.nombres_razSoc;
                mySqlParameter2.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter3 = new MySqlParameter(ListAtributos.p_apellidos_nomCom, MySqlDbType.VarChar);
                mySqlParameter3.Value = eNT_Persona.apellidos_nomCom;
                mySqlParameter3.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter4 = new MySqlParameter(ListAtributos.p_fecNac_iniActv, MySqlDbType.DateTime);
                mySqlParameter4.Value = eNT_Persona.fechaActv;
                mySqlParameter4.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter5 = new MySqlParameter(ListAtributos.p_idUsuaReg, MySqlDbType.Int32);
                mySqlParameter5.Value = eNT_Persona.idUsuaReg;
                mySqlParameter5.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter6 = new MySqlParameter(ListAtributos.p_esBanco, MySqlDbType.Int32);
                mySqlParameter6.Value = eNT_Persona.esBanco;
                mySqlParameter6.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter7 = new MySqlParameter(ListAtributos.p_esCliente, MySqlDbType.Int32);
                mySqlParameter7.Value = eNT_Persona.esCliente;
                mySqlParameter7.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter8 = new MySqlParameter(ListAtributos.p_esProveedor, MySqlDbType.Int32);
                mySqlParameter8.Value = eNT_Persona.esProveedor;
                mySqlParameter8.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter9 = new MySqlParameter(ListAtributos.p_esColaborador, MySqlDbType.Int32);
                mySqlParameter9.Value = eNT_Persona.esColaborador;
                mySqlParameter9.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter10 = new MySqlParameter(ListAtributos.p_esAfp, MySqlDbType.Int32);
                mySqlParameter10.Value = eNT_Persona.esAFP;
                mySqlParameter10.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter11 = new MySqlParameter(ListAtributos.p_esFamiliar, MySqlDbType.Int32);
                mySqlParameter11.Value = eNT_Persona.esFamiliar;
                mySqlParameter11.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter12 = new MySqlParameter(ListAtributos.p_esSocio, MySqlDbType.Int32);
                mySqlParameter12.Value = eNT_Persona.esSocio;
                mySqlParameter12.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter13 = new MySqlParameter(ListAtributos.p_fotoPersona, MySqlDbType.LongText);
                mySqlParameter13.Value = eNT_Persona.fotoPersona;
                mySqlParameter13.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameterSalida = new MySqlParameter(ListAtributos.p_id, MySqlDbType.Int32);
                mySqlParameterSalida.Direction = ParameterDirection.Output;

                mySqlCommand.Parameters.Add(mySqlParameter1);
                mySqlCommand.Parameters.Add(mySqlParameter2);
                mySqlCommand.Parameters.Add(mySqlParameter3);
                mySqlCommand.Parameters.Add(mySqlParameter4);
                mySqlCommand.Parameters.Add(mySqlParameter5);
                mySqlCommand.Parameters.Add(mySqlParameter6);
                mySqlCommand.Parameters.Add(mySqlParameter7);
                mySqlCommand.Parameters.Add(mySqlParameter8);
                mySqlCommand.Parameters.Add(mySqlParameter9);
                mySqlCommand.Parameters.Add(mySqlParameter10);
                mySqlCommand.Parameters.Add(mySqlParameter11);
                mySqlCommand.Parameters.Add(mySqlParameter12);
                mySqlCommand.Parameters.Add(mySqlParameter13);
                mySqlCommand.Parameters.Add(mySqlParameterSalida);

                mySqlCommand.ExecuteNonQuery();

                respuesta.idRespuesta = int.Parse(mySqlCommand.Parameters[ListAtributos.p_id].Value.ToString());

                switch (respuesta.idRespuesta)
                {
                    case -1: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorControladoInsertarPersona1; }; break;
                    case -2: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorControladoInsertarPersona2; }; break;
                    default: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaCorrectaInsert; }; break;
                }
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }

            return respuesta;
        }

    }
}
