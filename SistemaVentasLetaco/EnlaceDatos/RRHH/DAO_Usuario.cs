﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Entidades;
using MySql.Data.MySqlClient;

namespace EnlaceDatos
{
    [Serializable]
    public partial class DAO_Usuario
    {
        Conexion conexion;

        public DAO_Usuario()
        {
            conexion = new Conexion();
        }

        public Respuesta Insertar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();
            respuesta.idRespuesta = 0;
            respuesta.mensajeRespuesta = string.Empty;

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.RRHH_Insertar_Usuario, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_idPersona, MySqlDbType.Int32);
                mySqlParameter1.Value = eNT_Usuario.idPersona;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter2 = new MySqlParameter(ListAtributos.p_usuario, MySqlDbType.VarChar);
                mySqlParameter2.Value = eNT_Usuario.usuario;
                mySqlParameter2.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter3 = new MySqlParameter(ListAtributos.p_clave, MySqlDbType.VarChar);
                mySqlParameter3.Value = eNT_Usuario.clave;
                mySqlParameter3.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter4 = new MySqlParameter(ListAtributos.p_preguntaSeguridad, MySqlDbType.VarChar);
                mySqlParameter4.Value = eNT_Usuario.preguntaSeguridad;
                mySqlParameter4.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter5 = new MySqlParameter(ListAtributos.p_respuesta, MySqlDbType.VarChar);
                mySqlParameter5.Value = eNT_Usuario.respuesta;
                mySqlParameter5.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter6 = new MySqlParameter(ListAtributos.p_idUsuaReg, MySqlDbType.Int32);
                mySqlParameter6.Value = eNT_Usuario.idUsuaReg;
                mySqlParameter6.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameterSalida = new MySqlParameter(ListAtributos.p_id, MySqlDbType.Int32);
                mySqlParameterSalida.Direction = ParameterDirection.Output;

                mySqlCommand.Parameters.Add(mySqlParameter1);
                mySqlCommand.Parameters.Add(mySqlParameter2);
                mySqlCommand.Parameters.Add(mySqlParameter3);
                mySqlCommand.Parameters.Add(mySqlParameter4);
                mySqlCommand.Parameters.Add(mySqlParameter5);
                mySqlCommand.Parameters.Add(mySqlParameter6);
                mySqlCommand.Parameters.Add(mySqlParameterSalida);

                mySqlCommand.ExecuteNonQuery();

                respuesta.idRespuesta = int.Parse(mySqlCommand.Parameters[ListAtributos.p_id].Value.ToString());

                switch (respuesta.idRespuesta)
                {
                    case -1: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorControladoInsertarUsuario1; }; break;
                    case -2: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorControladoInsertarUsuario2; }; break;
                    default: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaCorrectaInsert; }; break;
                }
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }

            return respuesta;
        }

        public Respuesta Editar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.RRHH_Editar_Usuario, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];                

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_idPersona, MySqlDbType.Int32);
                mySqlParameter1.Value = eNT_Usuario.idPersona;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter2 = new MySqlParameter(ListAtributos.p_usuario, MySqlDbType.VarChar);
                mySqlParameter2.Value = eNT_Usuario.usuario;
                mySqlParameter2.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter3 = new MySqlParameter(ListAtributos.p_clave, MySqlDbType.VarChar);
                mySqlParameter3.Value = eNT_Usuario.clave;
                mySqlParameter3.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter4 = new MySqlParameter(ListAtributos.p_preguntaSeguridad, MySqlDbType.VarChar);
                mySqlParameter4.Value = eNT_Usuario.preguntaSeguridad;
                mySqlParameter4.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter5 = new MySqlParameter(ListAtributos.p_respuesta, MySqlDbType.VarChar);
                mySqlParameter5.Value = eNT_Usuario.respuesta;
                mySqlParameter5.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter6 = new MySqlParameter(ListAtributos.p_idUsuActv, MySqlDbType.Int32);
                mySqlParameter6.Value = eNT_Usuario.idUsuActv;
                mySqlParameter6.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter7 = new MySqlParameter(ListAtributos.p_idUsuario, MySqlDbType.Int32);
                mySqlParameter7.Value = eNT_Usuario.idUsuario;
                mySqlParameter7.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter8 = new MySqlParameter(ListAtributos.p_estado, MySqlDbType.Int32);
                mySqlParameter8.Value = eNT_Usuario.estado;
                mySqlParameter8.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameterSalida = new MySqlParameter(ListAtributos.p_id, MySqlDbType.Int32);
                mySqlParameterSalida.Direction = ParameterDirection.Output;

                mySqlCommand.Parameters.Add(mySqlParameter1);
                mySqlCommand.Parameters.Add(mySqlParameter2);
                mySqlCommand.Parameters.Add(mySqlParameter3);
                mySqlCommand.Parameters.Add(mySqlParameter4);
                mySqlCommand.Parameters.Add(mySqlParameter5);
                mySqlCommand.Parameters.Add(mySqlParameter6);
                mySqlCommand.Parameters.Add(mySqlParameter7);
                mySqlCommand.Parameters.Add(mySqlParameter8);
                mySqlCommand.Parameters.Add(mySqlParameterSalida);

                mySqlCommand.ExecuteNonQuery();

                respuesta.idRespuesta = int.Parse(mySqlCommand.Parameters[ListAtributos.p_id].Value.ToString());

                switch (respuesta.idRespuesta)
                {
                    case -1: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorControladoEditarUsuario1; }; break;
                    case -2: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorControladoEditarUsuario2; }; break;
                    default: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaCorrectaEditar; }; break;
                }
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }

            return respuesta;
        }

        public List<ENT_Usuario> Listar(params object[] valoresPermitidos)
        {
            List<ENT_Usuario> eNT_UsuarioListResponse = new List<ENT_Usuario>();

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.RRHH_Listar_Usuario, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_idUsuario, MySqlDbType.Int32);
                mySqlParameter1.Value = eNT_Usuario.idUsuario;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter2 = new MySqlParameter(ListAtributos.p_estado, MySqlDbType.Int32);
                mySqlParameter2.Value = eNT_Usuario.estado;
                mySqlParameter2.Direction = ParameterDirection.Input;

                mySqlCommand.Parameters.Add(mySqlParameter1);
                mySqlCommand.Parameters.Add(mySqlParameter2);

                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
                DataTable _dataTable = new DataTable();
                mySqlDataAdapter.Fill(_dataTable);

                if (_dataTable != null)
                {
                    foreach (DataRow item in _dataTable.Rows)
                    {
                        ENT_Usuario eNT_UsuarioEX = new ENT_Usuario();

                        eNT_UsuarioEX.idUsuario = int.Parse(item[0].ToString());
                        eNT_UsuarioEX.usuario = item[1].ToString();
                        eNT_UsuarioEX.estado = int.Parse(item[2].ToString());
                        eNT_UsuarioEX.fechaReg = DateTime.Parse(item[3].ToString());
                        eNT_UsuarioEX.descripcionEstado = item[4].ToString();
                        eNT_UsuarioEX.nombreUsuarioReg = item[5].ToString();
                        eNT_UsuarioEX.nombrePersona = item[6].ToString();
                        eNT_UsuarioEX.preguntaSeguridad = item[7].ToString();
                        eNT_UsuarioEX.respuesta = item[8].ToString();
                        eNT_UsuarioEX.clave = item[9].ToString();
                        eNT_UsuarioEX.idPersona = int.Parse(item[10].ToString());
                        eNT_UsuarioEX.respuestaTransaccion = new Respuesta() { idRespuesta = int.Parse(item[0].ToString()), mensajeRespuesta = ListAtributos.msj_respuestaCorrectaListar };

                        eNT_UsuarioListResponse.Add(eNT_UsuarioEX);
                    }
                }
            }
            catch (Exception ex)
            {
                ENT_Usuario eNT_UsuarioEX = new ENT_Usuario();
                eNT_UsuarioEX.respuestaTransaccion = new Respuesta() { mensajeRespuesta = ex.Message, idRespuesta = -1 };

                eNT_UsuarioListResponse.Add(eNT_UsuarioEX);
            }
            finally
            {
                mySqlConnection.Close();
            }

            return eNT_UsuarioListResponse;

        }

        public List<ENT_Usuario> ListarUsuarioCombo(params object[] valoresPermitidos)
        {
            List<ENT_Usuario> eNT_UsuarioListResponse = new List<ENT_Usuario>();

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.RRHH_Listar_Usuario_Combo, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];
                int tipoListado = (int)valoresPermitidos[1];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_tipoListado, MySqlDbType.Int32);
                mySqlParameter1.Value = tipoListado;
                mySqlParameter1.Direction = ParameterDirection.Input;

                mySqlCommand.Parameters.Add(mySqlParameter1);

                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
                DataTable _dataTable = new DataTable();
                mySqlDataAdapter.Fill(_dataTable);

                if (_dataTable != null)
                {
                    foreach (DataRow item in _dataTable.Rows)
                    {
                        ENT_Usuario eNT_UsuarioEX = new ENT_Usuario();

                        eNT_UsuarioEX.idUsuario = int.Parse(item[0].ToString());
                        eNT_UsuarioEX.usuario = item[1].ToString();
                        eNT_UsuarioEX.nombrePersona = item[2].ToString();
                        eNT_UsuarioEX.descripcionEstado = item[3].ToString();

                        eNT_UsuarioEX.respuestaTransaccion = new Respuesta() { idRespuesta = int.Parse(item[0].ToString()), mensajeRespuesta = ListAtributos.msj_respuestaCorrectaListar };

                        eNT_UsuarioListResponse.Add(eNT_UsuarioEX);
                    }
                }
            }
            catch (Exception ex)
            {
                ENT_Usuario eNT_UsuarioEX = new ENT_Usuario();
                eNT_UsuarioEX.respuestaTransaccion = new Respuesta() { mensajeRespuesta = ex.Message, idRespuesta = -1 };

                eNT_UsuarioListResponse.Add(eNT_UsuarioEX);
            }
            finally
            {
                mySqlConnection.Close();
            }

            return eNT_UsuarioListResponse;

        }

        public Respuesta Eliminar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();
            respuesta.idRespuesta = 0;
            respuesta.mensajeRespuesta = string.Empty;

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.RRHH_Eliminar_Usuario, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_idUsuario, MySqlDbType.Int32);
                mySqlParameter1.Value = eNT_Usuario.idUsuario;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter2 = new MySqlParameter(ListAtributos.p_idUsuaBaja, MySqlDbType.Int32);
                mySqlParameter2.Value = eNT_Usuario.idUsuaBaja;
                mySqlParameter2.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameterSalida = new MySqlParameter(ListAtributos.p_id, MySqlDbType.Int32);
                mySqlParameterSalida.Direction = ParameterDirection.Output;

                mySqlCommand.Parameters.Add(mySqlParameter1);
                mySqlCommand.Parameters.Add(mySqlParameter2);
                mySqlCommand.Parameters.Add(mySqlParameterSalida);

                mySqlCommand.ExecuteNonQuery();

                respuesta.idRespuesta = int.Parse(mySqlCommand.Parameters[ListAtributos.p_id].Value.ToString());               

                switch (respuesta.idRespuesta)
                {
                    case -1: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorControladoEliminarUsuario1; }; break;
                    case -2: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorControladoEliminarUsuario2; }; break;
                    default: { respuesta.mensajeRespuesta = ListAtributos.msj_respuestaCorrectaEliminar; }; break;
                }
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }
            return respuesta;
        }

        public ENT_Usuario IniciarSesion(params object[] valoresPermitidos)
        {
            ENT_Usuario eNT_UsuarioEX = new ENT_Usuario();
            Respuesta respuesta = new Respuesta();
            respuesta.idRespuesta = 0;
            respuesta.mensajeRespuesta = string.Empty;

            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.RRHH_Iniciar_Sesion, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];

                MySqlParameter mySqlParameter1 = new MySqlParameter(ListAtributos.p_usuario, MySqlDbType.VarChar);
                mySqlParameter1.Value = eNT_Usuario.usuario;
                mySqlParameter1.Direction = ParameterDirection.Input;

                MySqlParameter mySqlParameter2 = new MySqlParameter(ListAtributos.p_clave, MySqlDbType.VarChar);
                mySqlParameter2.Value = eNT_Usuario.clave;
                mySqlParameter2.Direction = ParameterDirection.Input;                

                mySqlCommand.Parameters.Add(mySqlParameter1);
                mySqlCommand.Parameters.Add(mySqlParameter2);

                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
                DataTable _dataTable = new DataTable();
                mySqlDataAdapter.Fill(_dataTable);
                

                if (_dataTable != null)
                {
                    foreach (DataRow item in _dataTable.Rows)
                    {
                        eNT_UsuarioEX.idUsuario = int.Parse(item[0].ToString());
                        eNT_UsuarioEX.usuario = item[1].ToString();
                        eNT_UsuarioEX.estado = int.Parse(item[2].ToString());
                        eNT_UsuarioEX.idPersona = int.Parse(item[3].ToString());
                        eNT_UsuarioEX.preguntaSeguridad = item[4].ToString();                        
                        eNT_UsuarioEX.respuestaTransaccion = new Respuesta() { idRespuesta = int.Parse(item[0].ToString()), mensajeRespuesta = ListAtributos.msj_respuestaCorrectaIniciarSesion };
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
                eNT_UsuarioEX.respuestaTransaccion = respuesta;
            }
            finally
            {
                mySqlConnection.Close();
            }

            return eNT_UsuarioEX;
        }
    }
}
