﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Entidades;
using MySql.Data.MySqlClient;


namespace EnlaceDatos
{
    [Serializable]
    public class DAO_TipoPersona
    {
        Conexion conexion;

        public DAO_TipoPersona()
        {
            conexion = new Conexion();
        }

        public List<ENT_TipoPersona> ListarComboTipoPersona(params object[] valoresPermitidos)
        {
            List<ENT_TipoPersona> eNT_TipoPersonaListResponse = new List<ENT_TipoPersona>();
            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.RRHH_Listar_TipoPersona_Combo, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_TipoPersona eNT_TipoPersona = (ENT_TipoPersona)valoresPermitidos[0];

                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
                DataTable _dataTable = new DataTable();
                mySqlDataAdapter.Fill(_dataTable);

                if (_dataTable != null)
                {
                    foreach (DataRow item in _dataTable.Rows)
                    {
                        ENT_TipoPersona eNT_TipoPersonaEX = new ENT_TipoPersona();

                        eNT_TipoPersonaEX.idTipoPersona = int.Parse(item[0].ToString());
                        eNT_TipoPersonaEX.descripcion = item[1].ToString();
                        eNT_TipoPersonaEX.Respuesta = new Respuesta() { idRespuesta = int.Parse(item[0].ToString()), mensajeRespuesta = ListAtributos.msj_respuestaCorrectaListar };
                        eNT_TipoPersonaListResponse.Add(eNT_TipoPersonaEX);
                    }
                }

            }
            catch (Exception ex)
            {
                ENT_TipoPersona eNT_TipoPersonaEX = new ENT_TipoPersona();
                eNT_TipoPersonaEX.Respuesta.mensajeRespuesta = ex.Message;
                eNT_TipoPersonaEX.Respuesta.idRespuesta = -1;

                eNT_TipoPersonaListResponse.Add(eNT_TipoPersonaEX);
            }
            finally
            {
                mySqlConnection.Close();
            }
            return eNT_TipoPersonaListResponse;

        }
    }
}
