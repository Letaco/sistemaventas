﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Entidades;
using MySql.Data.MySqlClient;


namespace EnlaceDatos
{
    [Serializable]
    public class DAO_TipoActividad
    {
        Conexion conexion;

        public DAO_TipoActividad()
        {
            conexion = new Conexion();
        }

        public List<ENT_TipoActividad> ListarComboTipoActividad(params object[] valoresPermitidos)
        {
            List<ENT_TipoActividad> eNT_TipoActividadListResponse = new List<ENT_TipoActividad>();
            MySqlConnection mySqlConnection = conexion.getConexion();
            mySqlConnection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(ListAtributos.EMPR_Listar_TipoActividad_Combo, mySqlConnection);
            mySqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ENT_TipoActividad eNT_TipoActividad = (ENT_TipoActividad)valoresPermitidos[0];

                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
                DataTable _dataTable = new DataTable();
                mySqlDataAdapter.Fill(_dataTable);

                if (_dataTable != null)
                {
                    foreach (DataRow item in _dataTable.Rows)
                    {
                        ENT_TipoActividad eNT_TipoActividadEX = new ENT_TipoActividad();

                        eNT_TipoActividadEX.idTipoActividad = int.Parse(item[0].ToString());
                        eNT_TipoActividadEX.codSunat = item[1].ToString();
                        eNT_TipoActividadEX.descripcion = item[2].ToString();
                        eNT_TipoActividadEX.Respuesta = new Respuesta() { idRespuesta = int.Parse(item[0].ToString()), mensajeRespuesta = ListAtributos.msj_respuestaCorrectaListar };
                        eNT_TipoActividadListResponse.Add(eNT_TipoActividadEX);
                    }
                }

            }
            catch (Exception ex)
            {
                ENT_TipoActividad eNT_TipoActividadEX = new ENT_TipoActividad();
                eNT_TipoActividadEX.Respuesta.mensajeRespuesta = ex.Message;
                eNT_TipoActividadEX.Respuesta.idRespuesta = -1;

                eNT_TipoActividadListResponse.Add(eNT_TipoActividadEX);
            }
            finally
            {
                mySqlConnection.Close();
            }
            return eNT_TipoActividadListResponse;

        }
    }
}
