﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnlaceDatos
{
    /// <summary>
    /// Lista de Procedimientos almacenados
    /// </summary>
    public static class ListAtributos
    {
        #region "Campos"

        public static string p_id = "p_id";
        public static string p_estado = "p_estado";
        public static string p_idUsuaReg = "p_idUsuaReg";
        public static string p_fechaReg = "p_fechaReg";
        public static string p_idUsuaBaja = "p_idUsuaBaja";
        public static string p_fechabaja = "p_fechabaja";
        public static string p_idUsuActv = "p_idUsuActv";
        public static string p_fechaActv = "p_fechaActv";
        public static string p_idCondicionDomiciliado = "p_idCondicionDomiciliado";
        public static string p_descripcion = "p_descripcion";
        public static string p_descripcionCorta = "p_descripcionCorta";
        public static string p_idNivelEducativo = "p_idNivelEducativo";
        public static string p_idOcupacionOficio = "p_idOcupacionOficio";
        public static string p_idOrigenEntidad = "p_idOrigenEntidad";
        public static string p_idTipoDocIdentidad = "p_idTipoDocIdentidad";
        public static string p_cifrasValidacion = "p_cifrasValidacion";
        public static string p_digitosExactos = "p_digitosExactos";
        public static string p_idTipoGenero = "p_idTipoGenero";
        public static string p_idTipoNacionalidad = "p_idTipoNacionalidad";
        public static string p_idTipoVia = "p_idTipoVia";
        public static string p_idTipoZona = "p_idTipoZona";
        public static string p_idUbigeo = "p_idUbigeo";
        public static string p_idUbigeoPadre = "p_idUbigeoPadre";
        public static string p_codigoSunatPadre = "p_codigoSunatPadre";
        public static string p_codSunat = "p_codSunat";
        public static string p_nivel = "p_nivel";
        public static string p_idCargo = "p_idCargo";
        public static string p_idEmpresa = "p_idEmpresa";
        public static string p_idSucursal = "p_idSucursal";
        public static string p_idTipoCargo = "p_idTipoCargo";
        public static string p_nombreCargo = "p_nombreCargo";
        public static string p_idPersona = "p_idPersona";
        public static string p_idTipoActividad = "p_idTipoActividad";
        public static string p_idTipoEmpresa = "p_idTipoEmpresa";
        public static string p_condicionEmpresa = "p_condicionEmpresa";
        public static string p_estadoEmpresa = "p_estadoEmpresa";
        public static string p_idColaborador = "p_idColaborador";
        public static string p_nombreSucursal = "p_nombreSucursal";
        public static string p_fotoSucursal = "p_fotoSucursal";
        public static string p_fechaInicio = "p_fechaInicio";
        public static string p_idContrato = "p_idContrato";
        public static string p_idPlantillaContrato = "p_idPlantillaContrato";
        public static string p_idtipoContrato = "p_idtipoContrato";
        public static string p_fechaFin = "p_fechaFin";
        public static string p_sueldoBruto = "p_sueldoBruto";
        public static string p_sueldoNeto = "p_sueldoNeto";
        public static string p_tituloContrato = "p_tituloContrato";
        public static string p_objetivoContrato = "p_objetivoContrato";
        public static string p_clausulas1 = "p_clausulas1";
        public static string p_clausulas2 = "p_clausulas2";
        public static string p_clausulas3 = "p_clausulas3";
        public static string p_idDireccionPersona = "p_idDireccionPersona";
        public static string p_idNatural = "p_idNatural";
        public static string p_tipoZona = "p_tipoZona";
        public static string p_tipoVia = "p_tipoVia";
        public static string p_direccion = "p_direccion";
        public static string p_numeroindicador = "p_numeroindicador";
        public static string p_idDocumentoPersona = "p_idDocumentoPersona";
        public static string p_idTipoDocumento = "p_idTipoDocumento";
        public static string p_numeroDocumento = "p_numeroDocumento";
        public static string p_idFotocheck = "p_idFotocheck";
        public static string p_nombres = "p_nombres";
        public static string p_apellidos = "p_apellidos";
        public static string p_codigoAutogenerado = "p_codigoAutogenerado";
        public static string p_imagenQR = "p_imagenQR";
        public static string p_imagenFotocheck = "p_imagenFotocheck";
        public static string p_idTipoPersona = "p_idTipoPersona";
        public static string p_nombres_razSoc = "p_nombres_razSoc";
        public static string p_apellidos_nomCom = "p_apellidos_nomCom";
        public static string p_fecNac_iniActv = "p_fecNac_iniActv";
        public static string p_fotoPersona = "p_fotoPersona";
        public static string p_nombre = "p_nombre";
        public static string p_determinaActores = "p_determinaActores";
        public static string p_idUsuario = "p_idUsuario";
        public static string p_usuario = "p_usuario";
        public static string p_clave = "p_clave";
        public static string p_preguntaSeguridad = "p_preguntaSeguridad";
        public static string p_respuesta = "p_respuesta";
        public static string p_tipoListado = "p_tipoListado";
        public static string p_esBanco = "p_esBanco";
        public static string p_esCliente = "p_esCliente";
        public static string p_esProveedor = "p_esProveedor";
        public static string p_esColaborador = "p_esColaborador";
        public static string p_esAfp = "p_esAfp";
        public static string p_esFamiliar = "p_esFamiliar";
        public static string p_esSocio = "p_esSocio";
        #endregion

        #region "Configuración"       

        //Condición Domiciliado
        public static string CONF_Listar_CondicionDomiciliado = "CONF_Listar_CondicionDomiciliado";
        public static string CONF_Insertar_CondicionDomiciliado = "CONF_Insertar_CondicionDomiciliado";
        public static string CONF_Editar_CondicionDomiciliado = "CONF_Editar_CondicionDomiciliado";

        //Nivel Educativo
        public static string CONF_Listar_NivelEducativo = "CONF_Listar_NivelEducativo";
        public static string CONF_Insertar_NivelEducativo = "CONF_Insertar_NivelEducativo";
        public static string CONF_Editar_NivelEducativo = "CONF_Editar_NivelEducativo";

        //Ocupación Oficio
        public static string CONF_Listar_OcupacionOficio = "CONF_Listar_OcupacionOficio";
        public static string CONF_Insertar_OcupacionOficio = "CONF_Insertar_OcupacionOficio";
        public static string CONF_Editar_OcupacionOficio = "CONF_Editar_OcupacionOficio";

        //Origen Entidad
        public static string CONF_Listar_OrigenEntidad = "CONF_Listar_OrigenEntidad";
        public static string CONF_Insertar_OrigenEntidad = "CONF_Insertar_OrigenEntidad";
        public static string CONF_Editar_OrigenEntidad = "CONF_Editar_OrigenEntidad";

        //Tipo Doc Identidad
        public static string CONF_Listar_TipoDocIdentidad = "CONF_Listar_TipoDocIdentidad";
        public static string CONF_Insertar_TipoDocIdentidad = "CONF_Insertar_TipoDocIdentidad";
        public static string CONF_Editar_TipoDocIdentidad = "CONF_Editar_TipoDocIdentidad";
        public static string CONF_Listar_TipoDocIdentidad_Combo = "CONF_Listar_TipoDocIdentidad_Combo";

        //Tipo Género
        public static string CONF_Listar_TipoGenero = "CONF_Listar_TipoGenero";
        public static string CONF_Insertar_TipoGenero = "CONF_Insertar_TipoGenero";
        public static string CONF_Editar_TipoGenero = "CONF_Editar_TipoGenero";

        //Tipo Nacionalidad
        public static string CONF_Listar_TipoNacionalidad = "CONF_Listar_TipoNacionalidad";
        public static string CONF_Insertar_TipoNacionalidad = "CONF_Insertar_TipoNacionalidad";
        public static string CONF_Editar_TipoNacionalidad = "CONF_Editar_TipoNacionalidad";

        //Tipo Vía
        public static string CONF_Listar_TipoVia = "CONF_Listar_TipoVia";
        public static string CONF_Insertar_TipoVia = "CONF_Insertar_TipoVia";
        public static string CONF_Editar_TipoVia = "CONF_Editar_TipoVia";

        //Tipo Zona
        public static string CONF_Listar_TipoZona = "CONF_Listar_TipoZona";
        public static string CONF_Insertar_TipoZona = "CONF_Insertar_TipoZona";
        public static string CONF_Editar_TipoZona = "CONF_Editar_TipoZona";

        //Ubigeo
        public static string CONF_Listar_Ubigeo = "CONF_Listar_Ubigeo";
        public static string CONF_Insertar_Ubigeo = "CONF_Insertar_Ubigeo";
        public static string CONF_Editar_Ubigeo = "CONF_Editar_Ubigeo";

        #endregion

        #region "Empresa"

        //Cargo
        public static string EMPR_Listar_Cargo = "EMPR_Listar_Cargo";
        public static string EMPR_Insertar_Cargo = "EMPR_Insertar_Cargo";
        public static string EMPR_Editar_Cargo = "EMPR_Editar_Cargo";

        //Empresa
        public static string EMPR_Listar_Empresa = "EMPR_Listar_Empresa";
        public static string EMPR_Insertar_Empresa = "EMPR_Insertar_Empresa";
        public static string EMPR_Editar_Empresa = "EMPR_Editar_Empresa";

        //Sucursal
        public static string EMPR_Listar_Sucursal = "EMPR_Listar_Sucursal";
        public static string EMPR_Insertar_Sucursal = "EMPR_Insertar_Sucursal";
        public static string EMPR_Editar_Sucursal = "EMPR_Editar_Sucursal";

        //Tipo Actividad
        public static string EMPR_Listar_TipoActividad = "EMPR_Listar_TipoActividad";
        public static string EMPR_Insertar_TipoActividad = "EMPR_Insertar_TipoActividad";
        public static string EMPR_Editar_TipoActividad = "EMPR_Editar_TipoActividad";
        public static string EMPR_Listar_TipoActividad_Combo = "EMPR_Listar_TipoActividad_Combo";

        //Tipo Cargo
        public static string EMPR_Listar_TipoCargo = "EMPR_Listar_TipoCargo";
        public static string EMPR_Insertar_TipoCargo = "EMPR_Insertar_TipoCargo";
        public static string EMPR_Editar_TipoCargo = "EMPR_Editar_TipoCargo";

        //Tipo Empresa
        public static string EMPR_Listar_TipoEmpresa = "EMPR_Listar_TipoEmpresa";
        public static string EMPR_Insertar_TipoEmpresa = "EMPR_Insertar_TipoEmpresa";
        public static string EMPR_Editar_TipoEmpresa = "EMPR_Editar_TipoEmpresa";

        #endregion

        #region "RRHH"

        //Colaborador
        public static string RRHH_Listar_Colaborador = "RRHH_Listar_Colaborador";
        public static string RRHH_Insertar_Colaborador = "RRHH_Insertar_Colaborador";        
        public static string RRHH_Editar_Colaborador = "RRHH_Editar_Colaborador";

        //Contrato
        public static string RRHH_Listar_Contrato = "RRHH_Listar_Contrato";
        public static string RRHH_Insertar_Contrato = "RRHH_Insertar_Contrato";
        public static string RRHH_Editar_Contrato = "RRHH_Editar_Contrato";

        //Dirección Entidad
        public static string RRHH_Listar_DireccionEntidad = "RRHH_Listar_DireccionEntidad";
        public static string RRHH_Insertar_DireccionEntidad = "RRHH_Insertar_DireccionEntidad";
        public static string RRHH_Editar_DireccionEntidad = "RRHH_Editar_DireccionEntidad";

        //Documento Persona
        public static string RRHH_Listar_DocumentoPersona = "RRHH_Listar_DocumentoPersona";
        public static string RRHH_Insertar_DocumentoPersona = "RRHH_Insertar_DocumentoPersona";
        public static string RRHH_Editar_DocumentoPersona = "RRHH_Editar_DocumentoPersona";

        //Fotocheck
        public static string RRHH_Listar_Fotocheck = "RRHH_Listar_Fotocheck";
        public static string RRHH_Insertar_Fotocheck = "RRHH_Insertar_Fotocheck";
        public static string RRHH_Editar_Fotocheck = "RRHH_Editar_Fotocheck";

        //Natural
        public static string RRHH_Listar_Natural = "RRHH_Listar_Natural";
        public static string RRHH_Insertar_Natural = "RRHH_Insertar_Natural";
        public static string RRHH_Editar_Natural = "RRHH_Editar_Natural";

        //Persona
        public static string RRHH_Listar_Persona = "RRHH_Listar_Persona";
        public static string RRHH_Insertar_Persona = "RRHH_Insertar_Persona";
        public static string RRHH_Editar_Persona = "RRHH_Editar_Persona";
        public static string RRHH_Listar_Persona_Combo = "RRHH_Listar_Persona_Combo";        

        //Plantilla Contrato
        public static string RRHH_Listar_PlantillaContrato = "RRHH_Listar_PlantillaContrato";
        public static string RRHH_Insertar_PlantillaContrato = "RRHH_Insertar_PlantillaContrato";
        public static string RRHH_Editar_PlantillaContrato = "RRHH_Editar_PlantillaContrato";

        //Tipo Contrato
        public static string RRHH_Listar_TipoContrato = "RRHH_Listar_TipoContrato";
        public static string RRHH_Insertar_TipoContrato = "RRHH_Insertar_TipoContrato";
        public static string RRHH_Editar_TipoContrato = "RRHH_Editar_TipoContrato";

        //Tipo Persona
        public static string RRHH_Listar_TipoPersona = "RRHH_Listar_TipoPersona";
        public static string RRHH_Insertar_TipoPersona = "RRHH_Insertar_TipoPersona";
        public static string RRHH_Editar_TipoPersona = "RRHH_Editar_TipoPersona";
        public static string RRHH_Listar_TipoPersona_Combo = "RRHH_Listar_TipoPersona_Combo";

        //Usuario
        public static string RRHH_Listar_Usuario = "RRHH_Listar_Usuario";
        public static string RRHH_Insertar_Usuario = "RRHH_Insertar_Usuario";
        public static string RRHH_Editar_Usuario = "RRHH_Editar_Usuario";
        public static string RRHH_Eliminar_Usuario = "RRHH_Eliminar_Usuario";
        public static string RRHH_Listar_Usuario_Combo = "RRHH_Listar_Usuario_Combo";
        public static string RRHH_Iniciar_Sesion = "RRHH_Iniciar_Sesion";


        #endregion

        #region "Mensajes"

        public static string msj_respuestaCorrectaInsert = "Registro(s) realizado(s) correctamente";
        public static string msj_respuestaCorrectaEditar = "Registro(s) editado(s) correctamente";
        public static string msj_respuestaCorrectaListar = "Registro(s) Listado(s) correctamente";
        public static string msj_respuestaCorrectaEliminar = "Registro(s) Eliminado(s) correctamente";
        public static string msj_respuestaCorrectaIniciarSesion = "Inicio de Sesión correcto";

        public static string msj_respuestaErrorValidacionBlancos = "Error - Existen campos que no deben ser vacíos";

        public static string msj_respuestaErrorControladoInsertarUsuario1 = "Error - Persona con usuario ya asignado";
        public static string msj_respuestaErrorControladoInsertarUsuario2 = "Error - Usuario ya existe";
        public static string msj_respuestaErrorControladoInsertarPersona1 = "Error - Persona ya existe";
        public static string msj_respuestaErrorControladoInsertarPersona2 = "Error - Datos de Persona no válidos";
        public static string msj_respuestaErrorControladoEliminarUsuario1 = "Error - Usuario no puede ser eliminado";
        public static string msj_respuestaErrorControladoEliminarUsuario2 = "Error - Usuario ya está fué eliminado";
        public static string msj_respuestaErrorControladoEditarUsuario1 = "No se encuentra Usuario";
        public static string msj_respuestaErrorControladoEditarUsuario2 = "Usuario no puede ser modificado";
        public static string msj_respuestaErrorControladoIniciarSesion1 = "Error en credenciales o estado inactivo";

        #endregion
    }
}
