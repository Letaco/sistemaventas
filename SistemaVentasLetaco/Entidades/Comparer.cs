﻿using System;
using System.Collections;

namespace Entidades
{
    public enum SortDirection
    {
        Asc = -1,
        Desc = 1
    }
    public class Comparer : IComparer
    {
        private string m_SortPropertyName;
        private SortDirection m_SortDirection;

        public Comparer(string sortPropertyName)
        {
            m_SortPropertyName = sortPropertyName;
            m_SortDirection = SortDirection.Asc;
        }

        public Comparer(string sortPropertyName, SortDirection sortDirection)
        {
            m_SortPropertyName = sortPropertyName;
            m_SortDirection = sortDirection;
        }

        public int Compare(object x, object y)
        {
            if ((m_SortPropertyName.Length < 1) | string.IsNullOrEmpty(m_SortPropertyName.Trim()))
                return 0;

            object valueOfX = x.GetType().GetProperty(m_SortPropertyName).GetValue(x, null);
            object valueOfY = y.GetType().GetProperty(m_SortPropertyName).GetValue(y, null);

            //System.IComparable comp = valueOfY; // as IComparable 
            System.IComparable comp = null;

            // Flip the value from whatever it was to the opposite.
            return Flip(comp.CompareTo(valueOfX));
        }

        private int Flip(int i)
        {
            return (i * System.Convert.ToInt32(m_SortDirection));
        }
    }
}
