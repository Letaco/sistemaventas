﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class ENT_Empresa
    {
        public int idEmpresa { get; set; }
        public int idPersona { get; set; }
        public int idTipoActividad { get; set; }
        public int idTipoEmpresa { get; set; }
        public int idCondicionDomiciliado { get; set; }
        public int idOrigenEntidad { get; set; }
        public int condicionEmpresa { get; set; }
        public int estadoEmpresa { get; set; }
        public int estado { get; set; }
        public int idUsuaReg { get; set; }
        public DateTime fechaReg { get; set; }
        public int idUsuaBaja { get; set; }
        public DateTime fechabaja { get; set; }
        public int idUsuActv { get; set; }
        public DateTime fechaActv { get; set; }

        //Datos adicionales
        public string nombreUsuarioReg { get; set; }
        public string nombreUsuarioBaja { get; set; }
        public string nombreUsuarioActv { get; set; }
        public string descripcionTipoActividad { get; set; }
        public string descripcionTipoEmpresa { get; set; }
        public string descripcionCondiciopnDomiciliado { get; set; }
        public string descripcionOrigenEntidad { get; set; }
        public string descripcionCondicionEmpresa { get; set; }
        public string descripcionEstadoEmpresa { get; set; }
        public Respuesta Respuesta { get; set; }
    }
}
