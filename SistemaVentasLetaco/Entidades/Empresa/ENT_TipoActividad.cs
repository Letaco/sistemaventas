﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class ENT_TipoActividad
    {
        public int idTipoActividad { get; set; }
        public string codSunat { get; set; }
        public string descripcion { get; set; }
        public int estado { get; set; }
        public int idUsuaReg { get; set; }
        public DateTime fechaReg { get; set; }
        public int idUsuaBaja { get; set; }
        public DateTime fechabaja { get; set; }
        public int idUsuActv { get; set; }
        public DateTime fechaActv { get; set; }

        //Datos adicionales
        public string nombreUsuarioReg { get; set; }
        public string nombreUsuarioBaja { get; set; }
        public string nombreUsuarioActv { get; set; }
        public string descripcionEstado { get; set; }
        public Respuesta Respuesta { get; set; }
    }
}
