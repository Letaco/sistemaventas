﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class ENT_DireccionEntidad
    {
        public int idDireccionPersona { get; set; }
        public int idPersona { get; set; }
        public int idEmpresa { get; set; }
        public int idSucursal { get; set; }
        public int idNatural { get; set; }
        public int idTipoZona { get; set; }
        public int idTipoVia { get; set; }
        public int idUbigeo { get; set; }
        public string tipoZona { get; set; }
        public string tipoVia { get; set; }
        public string direccion { get; set; }
        public string numeroindicador { get; set; }        
        public int estado { get; set; }
        public int idUsuaReg { get; set; }
        public DateTime fechaReg { get; set; }
        public int idUsuaBaja { get; set; }
        public DateTime fechabaja { get; set; }
        public int idUsuActv { get; set; }
        public DateTime fechaActv { get; set; }

        //Datos adicionales
        public string nombreUsuarioReg { get; set; }
        public string nombreUsuarioBaja { get; set; }
        public string nombreUsuarioActv { get; set; }
        public string descripcionEstado { get; set; }
        public string descripcionPersona { get; set; }        
        public string descripcionTipoZona { get; set; }
        public string descripcionTipoVia { get; set; }
        public string descripcionUbigeo { get; set; }
        public string descripcionSucursal { get; set; }
        public Respuesta Respuesta { get; set; }
    }
}
