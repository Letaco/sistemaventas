﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class ENT_DocumentoPersona
    {
        public int idDocumentoPersona { get; set; }
        public int idPersona { get; set; }
        public int idEmpresa { get; set; }
        public int idNatural { get; set; }
        public int idTipoDocumento { get; set; }
        public string numeroDocumento { get; set; }
        public int estado { get; set; }
        public int idUsuaReg { get; set; }
        public DateTime fechaReg { get; set; }
        public int idUsuaBaja { get; set; }
        public DateTime fechabaja { get; set; }
        public int idUsuActv { get; set; }
        public DateTime fechaActv { get; set; }

        //Datos adicionales
        public string nombreUsuarioReg { get; set; }
        public string nombreUsuarioBaja { get; set; }
        public string nombreUsuarioActv { get; set; }
        public string descripcionEstado { get; set; }
        public string descripcionPersona { get; set; }
        public string descripcionTipoDocumento { get; set; }
        public Respuesta Respuesta { get; set; }
    }
}
