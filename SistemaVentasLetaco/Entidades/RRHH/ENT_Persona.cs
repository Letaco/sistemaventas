﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class ENT_Persona
    {
        public int idPersona { get; set; }
        public int idTipoPersona { get; set; }
        public string nombres_razSoc { get; set; }
        public string apellidos_nomCom { get; set; }
        public DateTime fecNac_iniActv { get; set; }
        public string fotoPersona { get; set; }
        public int estado { get; set; }
        public int esBanco { get; set; }
        public int esCliente { get; set; }
        public int esProveedor { get; set; }
        public int esColaborador { get; set; }
        public int esFamiliar { get; set; }
        public int esSocio { get; set; }
        public int esAFP { get; set; }
        public int idUsuaReg { get; set; }
        public DateTime fechaReg { get; set; }
        public int idUsuaBaja { get; set; }
        public DateTime fechabaja { get; set; }
        public int idUsuActv { get; set; }
        public DateTime fechaActv { get; set; }

        //Datos adicionales
        public string nombreCompleto { get; set; }
        public string nombreUsuarioReg { get; set; }
        public string nombreUsuarioBaja { get; set; }
        public string nombreUsuarioActv { get; set; }
        public string descripcionEstado { get; set; }
        public string descripcionTipoPersona { get; set; }
        public Respuesta Respuesta { get; set; }

    }
}
