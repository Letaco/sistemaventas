﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Entidades
{
    [Serializable]
    public class ENT_Usuario
    {
        public int idUsuario { get; set; }
        public int idPersona { get; set; }
        public string usuario { get; set; }
        public string clave { get; set; }
        public string preguntaSeguridad { get; set; }
        public string respuesta { get; set; }
        public int estado { get; set; }
        public int idUsuaReg { get; set; }
        public DateTime fechaReg { get; set; }
        public int idUsuaBaja { get; set; }
        public DateTime fechabaja { get; set; }
        public int idUsuActv { get; set; }
        public DateTime fechaActv { get; set; }

        //Datos adicionales
        public string nombreUsuarioReg { get; set; }
        public string nombreUsuarioBaja { get; set; }
        public string nombreUsuarioActv { get; set; }
        public string nombrePersona { get; set; }
        public string descripcionEstado { get; set; }
        public Respuesta respuestaTransaccion { get; set; }
    }
}
