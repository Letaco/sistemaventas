﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class ENT_Contrato
    {
        public int idContrato { get; set; }
        public int idPlantillaContrato { get; set; }
        public int idtipoContrato { get; set; }
        public int idEmpresa { get; set; }
        public int idColaborador { get; set; }
        public int idCargo { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public decimal sueldoBruto { get; set; }
        public decimal sueldoNeto { get; set; }
        public string tituloContrato { get; set; }
        public string determinaActores { get; set; }
        public string objetivoContrato { get; set; }
        public string clausulas1 { get; set; }
        public string clausulas2 { get; set; }
        public string clausulas3 { get; set; }
        public int estado { get; set; }
        public int idUsuaReg { get; set; }
        public DateTime fechaReg { get; set; }
        public int idUsuaBaja { get; set; }
        public DateTime fechabaja { get; set; }
        public int idUsuActv { get; set; }
        public DateTime fechaActv { get; set; }

        //Datos adicionales
        public string nombreUsuarioReg { get; set; }
        public string nombreUsuarioBaja { get; set; }
        public string nombreUsuarioActv { get; set; }
        public string descripcionEstado { get; set; }
        public string descripcionPlanillaContrato { get; set; }
        public string descripcionTipoContrato { get; set; }
        public string descripcionEmpresa { get; set; }
        public string descripcionColaborador { get; set; }
        public string descripcionCargo { get; set; }
        public Respuesta Respuesta { get; set; }
    }
}
