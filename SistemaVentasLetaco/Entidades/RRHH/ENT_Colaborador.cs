﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class ENT_Colaborador
    {
        public int idColaborador { get; set; }
        public int idPersona { get; set; }
        public int idEmpresa { get; set; }
        public int idSucursal { get; set; }
        public int idCargo { get; set; }
        public int estado { get; set; }
        public int idUsuaReg { get; set; }
        public DateTime fechaReg { get; set; }
        public int idUsuaBaja { get; set; }
        public DateTime fechabaja { get; set; }
        public int idUsuActv { get; set; }
        public DateTime fechaActv { get; set; }

        //Datos adicionales
        public string nombreUsuarioReg { get; set; }
        public string nombreUsuarioBaja { get; set; }
        public string nombreUsuarioActv { get; set; }
        public string descripcionEstado { get; set; }
        public string descripcionEmpresa { get; set; }
        public string descripcionSucursal { get; set; }
        public string descripcionCargo { get; set; }
        public string descripcionPersona { get; set; }
        public Respuesta Respuesta { get; set; }
    }
}
