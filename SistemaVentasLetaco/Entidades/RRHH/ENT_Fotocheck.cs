﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class ENT_Fotocheck
    {
        public int idFotocheck { get; set; }
        public int idColaborador { get; set; }
        public int idCargo { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public string codigoAutogenerado { get; set; }
        public byte[] imagenQR { get; set; }
        public byte[] imagenFotocheck { get; set; }
        public int estado { get; set; }
        public int idUsuaReg { get; set; }
        public DateTime fechaReg { get; set; }
        public int idUsuaBaja { get; set; }
        public DateTime fechabaja { get; set; }
        public int idUsuActv { get; set; }
        public DateTime fechaActv { get; set; }

        //Datos adicionales
        public string nombreUsuarioReg { get; set; }
        public string nombreUsuarioBaja { get; set; }
        public string nombreUsuarioActv { get; set; }
        public string descripcionEstado { get; set; }
        public string descripcionColaborador { get; set; }
        public string descripcionCargo { get; set; }
        public Respuesta Respuesta { get; set; }
    }
}
