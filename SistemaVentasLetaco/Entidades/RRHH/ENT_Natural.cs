﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class ENT_Natural
    {
        public int idNatural { get; set; }
        public int idPersona { get; set; }
        public int idOcupacionOficio { get; set; }
        public int idTipoGenero { get; set; }
        public int idTipoNacionalidad { get; set; }        
        public int estado { get; set; }
        public int idUsuaReg { get; set; }
        public DateTime fechaReg { get; set; }
        public int idUsuaBaja { get; set; }
        public DateTime fechabaja { get; set; }
        public int idUsuActv { get; set; }
        public DateTime fechaActv { get; set; }

        //Datos adicionales
        public string nombreUsuarioReg { get; set; }
        public string nombreUsuarioBaja { get; set; }
        public string nombreUsuarioActv { get; set; }
        public string descripcionEstado { get; set; }
        public string descripcionPersona { get; set; }
        public string descripcionOcupacionOficio { get; set; }
        public string descripcionTipoGenero { get; set; }
        public string descripcionTipoNacionalidad { get; set; }
        public Respuesta Respuesta { get; set; }
    }
}
