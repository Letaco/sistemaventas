﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LETSHOP.Controllers
{
    public class Generales
    {
        public static string urlService = urlServiceDesa;
        public static string urlServiceDesa = Properties.Resources.ServiceDesa;
        public static string urlServiceTest = Properties.Resources.ServiceTest;
        public static string urlServiceProd = Properties.Resources.ServiceProd;

        public static string separador = "/";
    }
}
