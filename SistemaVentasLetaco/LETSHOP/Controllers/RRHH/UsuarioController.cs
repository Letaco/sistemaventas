﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using Entidades;
using EntidadesIntermedias;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace LETSHOP.Controllers
{
    [Route("[controller]")]
    public class UsuarioController : Controller
    {
        string controller = "usuario";
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult ListarUsuario()
        {
            try
            {
                UsuarioResponse usuarioResponse = new UsuarioResponse();
                string url = Generales.urlService + controller;
                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.ContentType = "application/json;charset-UTF-8";
                WebResponse webResponse = webRequest.GetResponse();
                using (var service = new StreamReader(webResponse.GetResponseStream()))
                {
                    usuarioResponse = JsonConvert.DeserializeObject<UsuarioResponse>(service.ReadToEnd().Trim());
                }

                foreach (ENT_Usuario item in usuarioResponse.eNT_UsuarioList)
                {
                    item.clave = desencriptarClave(item.clave);
                    item.preguntaSeguridad = (item.preguntaSeguridad == string.Empty) ? item.preguntaSeguridad : desencriptarClave(item.preguntaSeguridad);
                    item.respuesta = (item.respuesta == string.Empty) ? item.respuesta : desencriptarClave(item.respuesta);
                }

                return Json(JsonConvert.SerializeObject(usuarioResponse));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        [HttpGet("{idUsuario}/{estado}")]
        public JsonResult ListarUsuario(int idUsuario, int estado)
        {
            try
            {
                UsuarioResponse usuarioResponse = new UsuarioResponse();
                string url = Generales.urlService + controller + Generales.separador + idUsuario.ToString() + Generales.separador + estado.ToString();
                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.ContentType = "application/json;charset-UTF-8";
                WebResponse webResponse = webRequest.GetResponse();
                using (var service = new StreamReader(webResponse.GetResponseStream()))
                {
                    usuarioResponse = JsonConvert.DeserializeObject<UsuarioResponse>(service.ReadToEnd().Trim());
                }

                foreach (ENT_Usuario item in usuarioResponse.eNT_UsuarioList)
                {
                    item.clave = desencriptarClave(item.clave);
                    item.preguntaSeguridad = (item.preguntaSeguridad == string.Empty) ? item.preguntaSeguridad : desencriptarClave(item.preguntaSeguridad);
                    item.respuesta = (item.respuesta == string.Empty) ? item.respuesta : desencriptarClave(item.respuesta);
                }

                return Json(JsonConvert.SerializeObject(usuarioResponse));
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }

        [HttpGet]
        public JsonResult ListarUsuarioCombo(int tipoListado)
        {
            try
            {
                UsuarioResponse usuarioResponse = new UsuarioResponse();
                string metodo = "/combo/";
                string url = Generales.urlService + controller + metodo + tipoListado.ToString();
                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.ContentType = "application/json;charset-UTF-8";
                WebResponse webResponse = webRequest.GetResponse();
                using (var service = new StreamReader(webResponse.GetResponseStream()))
                {
                    usuarioResponse = JsonConvert.DeserializeObject<UsuarioResponse>(service.ReadToEnd().Trim());
                }
                return Json(JsonConvert.SerializeObject(usuarioResponse));
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        [HttpPost]
        public JsonResult InsertarUsuario([FromBody]ENT_Usuario eNT_Usuario)//string usuario, string clave, string pregunta, string respuesta, int idpersona)
        {
            try
            {
                Respuesta respuestaTransaccion = new Respuesta();
                string url = Generales.urlService + controller;
                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Method = WebRequestMethods.Http.Post;
                webRequest.ContentType = "application/json;charset-UTF-8";

                //ENT_Usuario datosUsuario = new ENT_Usuario();
                //datosUsuario.usuario = usuario;
                //datosUsuario.clave = encriptarClave(clave);
                //datosUsuario.preguntaSeguridad = (string.IsNullOrEmpty(pregunta)) ? string.Empty : encriptarClave(pregunta);
                //datosUsuario.respuesta = (string.IsNullOrEmpty(respuesta)) ? string.Empty : encriptarClave(respuesta);
                //datosUsuario.idPersona = idpersona;
                eNT_Usuario.clave = encriptarClave(eNT_Usuario.clave);
                eNT_Usuario.preguntaSeguridad = (string.IsNullOrEmpty(eNT_Usuario.preguntaSeguridad)) ? string.Empty : encriptarClave(eNT_Usuario.preguntaSeguridad);
                eNT_Usuario.respuesta = (string.IsNullOrEmpty(eNT_Usuario.respuesta)) ? string.Empty : encriptarClave(eNT_Usuario.respuesta);
                eNT_Usuario.idUsuaReg = 1;//Reemplazar por dato de sesión

                UsuarioRequest usuarioRequest = new UsuarioRequest();
                usuarioRequest.eNT_Usuario = eNT_Usuario;

                using (var serviceWrite = new StreamWriter(webRequest.GetRequestStream()))
                {
                    serviceWrite.Write(JsonConvert.SerializeObject(usuarioRequest));
                    serviceWrite.Flush();
                    serviceWrite.Close();
                }

                WebResponse webResponse = webRequest.GetResponse();
                using (var service = new StreamReader(webResponse.GetResponseStream()))
                {
                    respuestaTransaccion = JsonConvert.DeserializeObject<Respuesta>(service.ReadToEnd().Trim());
                }
                return Json(JsonConvert.SerializeObject(respuestaTransaccion));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("iniciarsesion")]
        public JsonResult IniciarSesion([FromBody]ENT_Usuario eNT_Usuario)
        {
            try
            {
                UsuarioResponse usuarioResponse = new UsuarioResponse();
                eNT_Usuario.idUsuaReg = 1;//Reemplazar por dato de sesión
                eNT_Usuario.clave = encriptarClave(eNT_Usuario.clave);

                string metodo = controller + Generales.separador +"iniciarsesion";
                string url = Generales.urlService + metodo;

                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var body = JsonConvert.SerializeObject(eNT_Usuario);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse<UsuarioResponse> response = client.Execute<UsuarioResponse>(request);
                usuarioResponse = JsonConvert.DeserializeObject<UsuarioResponse>(response.Content);

                return Json(JsonConvert.SerializeObject(usuarioResponse));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        public JsonResult EditarUsuario(int idUsuario, string usuario, string clave, string pregunta, string respuesta, int idpersona, int estado)
        {
            try
            {
                Respuesta respuestaTransaccion = new Respuesta();
                string url = Generales.urlService + controller + Generales.separador + idUsuario.ToString();
                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Method = WebRequestMethods.Http.Put;
                webRequest.ContentType = "application/json;charset-UTF-8";

                ENT_Usuario datosUsuario = new ENT_Usuario();
                datosUsuario.idUsuario = idUsuario;
                datosUsuario.usuario = usuario;
                datosUsuario.idPersona = idpersona;
                datosUsuario.clave = encriptarClave(clave);
                datosUsuario.preguntaSeguridad = (string.IsNullOrEmpty(pregunta)) ? string.Empty : encriptarClave(pregunta);
                datosUsuario.respuesta = (string.IsNullOrEmpty(respuesta)) ? string.Empty : encriptarClave(respuesta);
                datosUsuario.estado = estado;
                datosUsuario.idUsuActv = 1;//Reemplazar por dato de sesión

                UsuarioRequest usuarioRequest = new UsuarioRequest();
                usuarioRequest.eNT_Usuario = datosUsuario;

                using (var serviceWrite = new StreamWriter(webRequest.GetRequestStream()))
                {
                    serviceWrite.Write(JsonConvert.SerializeObject(usuarioRequest));
                    serviceWrite.Flush();
                    serviceWrite.Close();
                }

                WebResponse webResponse = webRequest.GetResponse();
                using (var service = new StreamReader(webResponse.GetResponseStream()))
                {
                    respuestaTransaccion = JsonConvert.DeserializeObject<Respuesta>(service.ReadToEnd().Trim());
                }
                return Json(JsonConvert.SerializeObject(respuestaTransaccion));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        public JsonResult EliminarUsuario(int idUsuario)
        {
            try
            {
                Respuesta respuestaTransaccion = new Respuesta();
                int idUsuaBaja = idUsuario; //sato será reemplazado por usuario de sesión
                string url = Generales.urlService + controller + Generales.separador + idUsuario.ToString() + Generales.separador + idUsuaBaja.ToString();
                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Method = "DELETE";
                webRequest.ContentType = "application/json;charset-UTF-8";
                WebResponse webResponse = webRequest.GetResponse();
                using (var service = new StreamReader(webResponse.GetResponseStream()))
                {
                    respuestaTransaccion = JsonConvert.DeserializeObject<Respuesta>(service.ReadToEnd().Trim());
                }
                return Json(JsonConvert.SerializeObject(respuestaTransaccion));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string encriptarClave(string clavein)
        {
            string letras = "#$%&=<6789abcdefghijklmnABCDEGFHIJKLMN,ñopqrstuvwxyzÑOPQRSTUVWXYZ12345?;._@:-{}[]+-*/ 0¡|!>";

            string claveEncript = "";

            foreach (char letra in clavein)
            {
                int x = letras.IndexOf(letra);
                claveEncript += x.ToString() + "L";
            }
            claveEncript += "T";
            claveEncript = claveEncript.Replace("LT", string.Empty);

            return claveEncript;

        }

        public string desencriptarClave(string claveEncript)
        {
            string letras = "#$%&=<6789abcdefghijklmnABCDEGFHIJKLMN,ñopqrstuvwxyzÑOPQRSTUVWXYZ12345?;._@:-{}[]+-*/ 0¡|!>";
            string claveIn = "";

            try
            {
                foreach (string letra in claveEncript.Split('L').ToList())
                {
                    int x = int.Parse(letra);
                    claveIn += letras.ElementAt(x);
                }
            }
            catch (Exception)
            {
                claveIn = claveEncript;
            }

            return claveIn;

        }
    }
}
