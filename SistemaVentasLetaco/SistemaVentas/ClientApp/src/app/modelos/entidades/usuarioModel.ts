import { RespuestaModel } from "./respuestaModel";

export class UsuarioModel {
  idUsuario?:number;
  isPersona?:number;
  usuario?:string;
  clave?:string;
  preguntaSeguridad?:string;
  respuesta?:string;
  estado?:number;
  idUsuaReg?:number;
  fechaReg?:Date;
  idUsuaBaja?:number;
  fechaBaja?:Date;
  idUsuActv?:number;
  fechaActv?:Date;

  //Datos Adicionales
  nombreUsuarioReg?:string;
  nombreUsuarioBaja?:string;
  nombreUsuarioActv?:string;
  nombrePersona?:string;
  descripcionEstado?:string;
  respuestaTransaccion?:RespuestaModel;
 }
