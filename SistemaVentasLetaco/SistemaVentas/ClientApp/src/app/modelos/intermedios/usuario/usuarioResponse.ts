import {UsuarioModel} from '../../entidades/usuarioModel'
import {RespuestaModel} from '../../entidades/respuestaModel'
export class UsuarioResponseModel {
    eNT_Usuario?:UsuarioModel;
    eNT_UsuarioList?:UsuarioModel[];
    respuestaTransaccion?:RespuestaModel;
 }