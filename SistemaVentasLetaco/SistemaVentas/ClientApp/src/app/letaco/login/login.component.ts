import { Component } from '@angular/core';
import { UsuarioService} from 'src/app/servicios/usuario.service';
import { UsuarioModel } from 'src/app/modelos/entidades/usuarioModel';
import { UsuarioResponseModel } from 'src/app/modelos/intermedios/usuario/usuarioResponse';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})

export class LoginComponent {
  public labelUsuario: string = 'Usuario';
  public labelContrasenia: string = 'Contraseña';  
  public btnIniciarSesion: string = 'Iniciar Sesión';
  public maximousuario:number = 15;
  public maximocontrasenia:number = 20;
  login:UsuarioModel;
  usuarioRespuesta:UsuarioModel;

  constructor(
    private urlService: UsuarioService
  ){
    this.login= new UsuarioModel();
    this.usuarioRespuesta = new UsuarioModel();
  }


  public Login(){    
    this.urlService.iniciarSesion(this.login).subscribe(response =>{
      this.usuarioRespuesta = response.eNT_Usuario;
    });
    console.log(this.usuarioRespuesta);
  }
}