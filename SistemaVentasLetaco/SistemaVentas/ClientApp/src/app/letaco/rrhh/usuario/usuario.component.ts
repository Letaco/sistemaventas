import { Component } from '@angular/core';

@Component({
  selector: 'usuario-app',
  templateUrl: "./usuario.component.html",
  styleUrls: ['./usuario.component.css']
})

export class usuarioComponent {
  idUsuario: number = 0;
  idPersona: number = 0;
  usuario: string = '';
  clave: string = '';
  preguntaSeguridad: string = '';
  respuesta: string = '';
  estado: number = 1;

  //Datos adicionales
  nombreUsuarioReg: string = '';
  nombreUsuarioBaja: string = '';
  nombreUsuarioActv: string = '';
  nombrePersona: string = '';
  descripcionEstado: string = '';
}
