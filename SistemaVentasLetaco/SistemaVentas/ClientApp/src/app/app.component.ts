import { Component } from '@angular/core';
import { UsuarioModel } from './modelos/entidades/usuarioModel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'Sistema de Ventas';
  usuariologueado?:UsuarioModel;
}
