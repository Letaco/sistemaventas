import { Injectable } from '@angular/core';
import { UsuarioModel } from 'src/app/modelos/entidades/usuarioModel';
import { UsuarioResponseModel } from 'src/app/modelos/intermedios/usuario/usuarioResponse';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { timeout } from 'rxjs/operators';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization':'my-auth-token'
  })
}

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  public timeout: number = 30000;
  private URL_SERVER: string = this.getAbsolutePath();
  public CONTROLLER:string = "usuario";
  public LOGIN_USUARIO: string = this.URL_SERVER + this.CONTROLLER + "/iniciarsesion";
  public LISTAR_USUARIO_1: string = this.URL_SERVER + this.CONTROLLER;

  constructor(private httpCliente: HttpClient) { }

  public iniciarSesion(usuario:UsuarioModel):Observable<UsuarioResponseModel>{
    return this.httpCliente.post<UsuarioResponseModel>(this.LOGIN_USUARIO, usuario ,httpOptions);
  }

  public listarUsuario():Observable<UsuarioResponseModel[]>{
    return this.httpCliente.get<UsuarioResponseModel[]>(this.LISTAR_USUARIO_1,httpOptions);
  }
  

  getAbsolutePath(): string {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    var URL = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
    //URL = environment.apiURL.registroURL;
    return URL;
  }
}
