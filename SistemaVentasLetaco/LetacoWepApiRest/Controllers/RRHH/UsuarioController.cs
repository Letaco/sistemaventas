﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.Serialization.Json;
using EntidadesIntermedias;
using Newtonsoft.Json;
using System.Text.Json;
using Entidades;
using EnlacePresentacion;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LetacoWepApiRest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class UsuarioController : ControllerBase
    {
        // GET: api/<UsuarioController>
        [HttpGet]
        public IActionResult Get()
        {
            UsuarioResponse usuarioResponse = new UsuarioResponse();
            Respuesta respuestaTransaccion;
            try
            {
                int idUsuario = 0;
                int estado = 0;
                UsuarioRequest usuarioRequest = new UsuarioRequest();
                usuarioRequest.eNT_Usuario = new ENT_Usuario() { idUsuario = idUsuario, estado = estado };

                EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();

                usuarioResponse = ePBL_Usuario.Listar(usuarioRequest);

                return Ok(JsonConvert.SerializeObject(usuarioResponse));
            }
            catch (Exception ex)
            {
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = "Error - " + ex.Message };
                usuarioResponse.respuestaTransaccion = respuestaTransaccion;
                return NotFound(JsonConvert.SerializeObject(usuarioResponse));
            }
        }

        // GET api/<UsuarioController>/5
        [HttpGet("{idUsuario}/{estado}")]
        public IActionResult Get(int idUsuario, int estado)
        {
            UsuarioResponse usuarioResponse = new UsuarioResponse();
            Respuesta respuestaTransaccion;
            try
            {
                UsuarioRequest usuarioRequest = new UsuarioRequest();
                usuarioRequest.eNT_Usuario = new ENT_Usuario() { idUsuario = idUsuario, estado = estado };

                EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();

                usuarioResponse = ePBL_Usuario.Listar(usuarioRequest);                

                return Ok(JsonConvert.SerializeObject(usuarioResponse));
            }
            catch (Exception ex)
            {
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = "Error - " + ex.Message };
                usuarioResponse.respuestaTransaccion = respuestaTransaccion;
                return NotFound(JsonConvert.SerializeObject(usuarioResponse));
            }
        }

        [HttpGet("combo/{tipoListado}")]
        public IActionResult ListarUsuarioCombo(int tipoListado)
        {
            UsuarioResponse usuarioResponse = new UsuarioResponse();
            Respuesta respuestaTransaccion;
            try
            {
                UsuarioRequest usuarioRequest = new UsuarioRequest();
                usuarioRequest.tipoListado = tipoListado;

                EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();

                usuarioResponse = ePBL_Usuario.ListarUsuarioCombo(usuarioRequest);

                respuestaTransaccion = usuarioResponse.respuestaTransaccion;

                usuarioResponse.respuestaTransaccion = respuestaTransaccion;

                return Ok(JsonConvert.SerializeObject(usuarioResponse));
            }
            catch (Exception ex)
            {
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = "Error - " + ex.Message };
                usuarioResponse.respuestaTransaccion = respuestaTransaccion;
                return NotFound(JsonConvert.SerializeObject(usuarioResponse));
            }            
        }

        // POST api/<UsuarioController>
        [HttpPost]
        public IActionResult Post([FromBody]UsuarioRequest usuarioRequest)
        {
            Respuesta respuestaTransaccion;
            try
            {
                //UsuarioRequest usuarioRequest = new UsuarioRequest();
                //usuarioRequest = JsonConvert.DeserializeObject<UsuarioRequest>(value.Trim());

                UsuarioResponse usuarioResponse = new UsuarioResponse();

                EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();

                usuarioResponse = ePBL_Usuario.Insertar(usuarioRequest);

                respuestaTransaccion = usuarioResponse.respuestaTransaccion;

                return Ok(JsonConvert.SerializeObject(respuestaTransaccion));
            }
            catch (Exception ex)
            {
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = "Error - " + ex.Message };
                return NotFound(JsonConvert.SerializeObject(respuestaTransaccion));
            }
        }

        [HttpPost("iniciarsesion")]
        public IActionResult IniciarSesion([FromBody]ENT_Usuario eNT_Usuario)
        {
            Respuesta respuestaTransaccion;
            UsuarioResponse usuarioResponse = new UsuarioResponse();
            try
            {
                EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();
                UsuarioRequest usuarioRequest = new UsuarioRequest();
                usuarioRequest.eNT_Usuario = eNT_Usuario;
                usuarioResponse = ePBL_Usuario.IniciarSesion(usuarioRequest);

                return Ok(JsonConvert.SerializeObject(usuarioResponse));
                
            }
            catch (Exception ex)
            {
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = "Error - " + ex.Message };
                usuarioResponse.respuestaTransaccion = respuestaTransaccion;
                return NotFound(JsonConvert.SerializeObject(usuarioResponse));
            }
        }

        // PUT api/<UsuarioController>/5
        [HttpPut]
        public IActionResult Put([FromBody]UsuarioRequest usuarioRequest)
        {
            Respuesta respuestaTransaccion;
            try
            {
                //UsuarioRequest usuarioRequest = new UsuarioRequest();
                //usuarioRequest = JsonConvert.DeserializeObject<UsuarioRequest>(value.Trim());
                //usuarioRequest.eNT_Usuario.idUsuario = idUsuario;
                UsuarioResponse usuarioResponse = new UsuarioResponse();                

                EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();

                usuarioResponse = ePBL_Usuario.Editar(usuarioRequest);

                respuestaTransaccion = usuarioResponse.respuestaTransaccion;

                return Ok(JsonConvert.SerializeObject(respuestaTransaccion));
            }
            catch (Exception ex)
            {
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = "Error - " + ex.Message };
                return NotFound(JsonConvert.SerializeObject(respuestaTransaccion));
            }
        }

        // DELETE api/<UsuarioController>/5
        [HttpDelete("{idUsuario}/{idUsuaBaja}")]
        public IActionResult Delete(int idUsuario, int idUsuaBaja)
        {
            Respuesta respuestaTransaccion;
            try
            {
                ENT_Usuario datosUsuario = new ENT_Usuario();
                datosUsuario.idUsuario = idUsuario;
                datosUsuario.idUsuaBaja = idUsuaBaja;

                UsuarioRequest usuarioRequest = new UsuarioRequest();
                usuarioRequest.eNT_Usuario = datosUsuario;

                UsuarioResponse usuarioResponse = new UsuarioResponse();

                EPBL_Usuario ePBL_Usuario = new EPBL_Usuario();

                usuarioResponse = ePBL_Usuario.Eliminar(usuarioRequest);

                respuestaTransaccion = usuarioResponse.respuestaTransaccion;

                return Ok(JsonConvert.SerializeObject(respuestaTransaccion));
            }
            catch (Exception ex)
            {
                respuestaTransaccion = new Respuesta() { idRespuesta = -1, mensajeRespuesta = "Error - " + ex.Message };
                return NotFound(JsonConvert.SerializeObject(respuestaTransaccion));
            }
        }

    }
}
