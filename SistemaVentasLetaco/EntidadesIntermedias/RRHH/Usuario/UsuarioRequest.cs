﻿using System;
using System.Collections.Generic;
using System.Text;
using Entidades;

namespace EntidadesIntermedias
{
    [Serializable]
    public class UsuarioRequest
    {

        public ENT_Usuario eNT_Usuario;

        public List<ENT_Usuario> eNT_UsuarioList;

        public int tipoListado;

    }
}
