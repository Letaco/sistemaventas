﻿using System;
using System.Collections.Generic;
using System.Text;
using Entidades;

namespace EntidadesIntermedias
{
    public class TipoPersonaResponse
    {
        public ENT_TipoPersona eNT_TipoPersona;

        public List<ENT_TipoPersona> eNT_TipoPersonaList;

        public Respuesta respuestaTransaccion;
    }
}
