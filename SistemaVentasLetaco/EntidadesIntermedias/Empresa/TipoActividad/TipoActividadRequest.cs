﻿using System;
using System.Collections.Generic;
using System.Text;
using Entidades;
namespace EntidadesIntermedias
{
    public class TipoActividadRequest
    {
        public ENT_TipoActividad eNT_TipoActividad;

        public List<ENT_TipoActividad> eNT_TipoActividadList;
    }
}
