﻿using System;
using System.Collections.Generic;
using System.Text;
using Entidades;

namespace EntidadesIntermedias
{
    public class TipoActividadResponse
    {
        public ENT_TipoActividad eNT_TipoActividad;

        public List<ENT_TipoActividad> eNT_TipoActividadList;

        public Respuesta respuestaTransaccion;
    }
}
