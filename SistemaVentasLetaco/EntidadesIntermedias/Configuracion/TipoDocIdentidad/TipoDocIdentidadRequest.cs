﻿using System;
using System.Collections.Generic;
using System.Text;
using Entidades;

namespace EntidadesIntermedias
{
    public class TipoDocIdentidadRequest
    {
        public ENT_TipoDocIdentidad eNT_TipoDocIdentidad;

        public List<ENT_TipoDocIdentidad> eNT_TipoDocIdentidadList;
    }
}
