﻿using System;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;

namespace ServiciosConsultaLetaco
{
    public class ConsultaDNI
    {
        public enum Result
        {
            Ok = 0,
            NoResul = 1,
            ErrorCapcha = 2,
            xError = 3
        }

        CookieContainer myCookie;
        bool development;
        string respuesta;
        Result estado;

        public ConsultaDNI() {
            //this.myCookie = new CookieContainer();
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.SystemDefault;
            this.development = true;
        }

        private bool ValidarCertificado(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (development) return true;
            return sslPolicyErrors == SslPolicyErrors.None;
        }

        /// <summary>
        /// Devuelve resultado de validación de DNI
        /// </summary>
        /// <returns>Texto de respuesta después de realizar busqueda de DNI</returns>
        public string getRespuesta()
        {
            return this.respuesta;
        }

        /// <summary>
        /// Devuelve un valor que indica si la validadción fue correcta o no
        /// </summary>
        /// <returns>Valor booleano que indica si fué correcto o no</returns>
        public bool getEstado()
        {
            return this.estado == Result.Ok;
        }

        /// <summary>
        /// Valida que el DNI ingresado sea correcto
        /// </summary>
        /// <param name="DNI">DNI persona</param>
        /// <returns>Nombre completo de persona</returns>
        public string DatosReniec_Solo_nombre(string DNI)
        {
            try
            {
                string myurl = string.Format("https://dni.optimizeperu.com/api/persons/{0}", DNI);                
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidarCertificado);
                HttpWebRequest myhttpWebRequest = HttpWebRequest.CreateHttp(myurl);
                myhttpWebRequest.Headers.Add("Authorization", "Bearer k4d2956bd531ab61d44f4fa07304b20e13913815");
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myhttpWebRequest.GetResponse();
                Stream mystream = myHttpWebResponse.GetResponseStream();
                StreamReader mystreamReader = new StreamReader(mystream);

                string xdatos = HttpUtility.HtmlDecode(mystreamReader.ReadToEnd());

                EntidadRENIEC persona = (EntidadRENIEC)JsonConvert.DeserializeObject(xdatos, typeof(EntidadRENIEC));
                string nombrepersona = persona.name + " " + persona.first_name + " " + persona.last_name;

                this.respuesta = "Datos Obtenidos correctamente";
                this.estado = Result.Ok;

                return nombrepersona.Trim();
            }
            catch (Exception ex)
            {
                this.respuesta = ex.Message;
                this.estado = Result.xError;
                return string.Empty;
            }

        }

        /// <summary>
        /// Valida que el DNI ingresado sea correcto
        /// </summary>
        /// <param name="DNI">DNI persona</param>
        /// <returns>Entidad persona</returns>
        public EntidadRENIEC DatosReniec(string DNI)
        {
            EntidadRENIEC persona;
            try
            {
                string myurl = string.Format("https://dni.optimizeperu.com/api/prod/persons/{0}", DNI);
                //string myurl = string.Format("https://dni.optimizeperu.com/?csrfmiddlewaretoken={0}&dni={1}", "P8ZPk9wnlNdvEr0WXuRc5maujyXGSplFCm12O8Eqj2sSfKKFFwGGz4ESFvgKfOpJ", DNI);
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidarCertificado);
                HttpWebRequest myhttpWebRequest = HttpWebRequest.CreateHttp(myurl);
                //myhttpWebRequest.CookieContainer = myCookie;
                //myhttpWebRequest.Proxy = null;
                //myhttpWebRequest.Credentials = CredentialCache.DefaultCredentials;
                myhttpWebRequest.Headers.Add("Authorization", "Bearer k4d2956bd531ab61d44f4fa07304b20e13913815");
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myhttpWebRequest.GetResponse();
                Stream mystream = myHttpWebResponse.GetResponseStream();
                StreamReader mystreamReader = new StreamReader(mystream);

                string xdatos = HttpUtility.HtmlDecode(mystreamReader.ReadToEnd());

                persona = (EntidadRENIEC)JsonConvert.DeserializeObject(xdatos, typeof(EntidadRENIEC));
                persona.resultado = "1";
                persona.mensaje = "Datos cargados correctamente";
                
            }
            catch (Exception ex)
            {
                persona = new EntidadRENIEC();
                persona.resultado = "-1";
                persona.mensaje = ex.Message;
            }

            return persona;

        }

    }

    public class EntidadRENIEC {
        public string dni { get; set; }
        public string name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string cui { get; set; }
        public string resultado { get; set; }
        public string mensaje { get; set; }
    }

}
