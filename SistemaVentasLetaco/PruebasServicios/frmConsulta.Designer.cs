﻿namespace ServiciosConsultaLetaco
{
    partial class frmConsulta
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsulta));
            this.btnConsultar = new System.Windows.Forms.Button();
            this.txtRUC = new System.Windows.Forms.TextBox();
            this.pbxImageRUC = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pbxActualizar = new System.Windows.Forms.PictureBox();
            this.txtcapcha = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnConsultaDNI = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDNI = new System.Windows.Forms.TextBox();
            this.txtResultDNI = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTC = new System.Windows.Forms.TextBox();
            this.btnConsultarTC = new System.Windows.Forms.Button();
            this.txtVenta = new System.Windows.Forms.TextBox();
            this.txtCompra = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImageRUC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxActualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConsultar
            // 
            this.btnConsultar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsultar.Location = new System.Drawing.Point(136, 130);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(87, 44);
            this.btnConsultar.TabIndex = 0;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // txtRUC
            // 
            this.txtRUC.Location = new System.Drawing.Point(16, 39);
            this.txtRUC.Name = "txtRUC";
            this.txtRUC.Size = new System.Drawing.Size(207, 23);
            this.txtRUC.TabIndex = 1;
            // 
            // pbxImageRUC
            // 
            this.pbxImageRUC.Cursor = System.Windows.Forms.Cursors.No;
            this.pbxImageRUC.Location = new System.Drawing.Point(16, 68);
            this.pbxImageRUC.Name = "pbxImageRUC";
            this.pbxImageRUC.Size = new System.Drawing.Size(146, 56);
            this.pbxImageRUC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImageRUC.TabIndex = 2;
            this.pbxImageRUC.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "RUC";
            // 
            // pbxActualizar
            // 
            this.pbxActualizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxActualizar.Image = ((System.Drawing.Image)(resources.GetObject("pbxActualizar.Image")));
            this.pbxActualizar.Location = new System.Drawing.Point(168, 68);
            this.pbxActualizar.Name = "pbxActualizar";
            this.pbxActualizar.Size = new System.Drawing.Size(55, 56);
            this.pbxActualizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxActualizar.TabIndex = 4;
            this.pbxActualizar.TabStop = false;
            this.pbxActualizar.Click += new System.EventHandler(this.pbxActualizar_Click);
            // 
            // txtcapcha
            // 
            this.txtcapcha.Location = new System.Drawing.Point(16, 138);
            this.txtcapcha.Name = "txtcapcha";
            this.txtcapcha.Size = new System.Drawing.Size(114, 23);
            this.txtcapcha.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(238, 21);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(658, 488);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.Text = "dataGridView1";
            // 
            // btnConsultaDNI
            // 
            this.btnConsultaDNI.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsultaDNI.Location = new System.Drawing.Point(136, 212);
            this.btnConsultaDNI.Name = "btnConsultaDNI";
            this.btnConsultaDNI.Size = new System.Drawing.Size(87, 44);
            this.btnConsultaDNI.TabIndex = 0;
            this.btnConsultaDNI.Text = "Consultar";
            this.btnConsultaDNI.UseVisualStyleBackColor = true;
            this.btnConsultaDNI.Click += new System.EventHandler(this.btnConsultaDNI_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 212);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "DNI";
            // 
            // txtDNI
            // 
            this.txtDNI.Location = new System.Drawing.Point(16, 230);
            this.txtDNI.Name = "txtDNI";
            this.txtDNI.Size = new System.Drawing.Size(102, 23);
            this.txtDNI.TabIndex = 1;
            // 
            // txtResultDNI
            // 
            this.txtResultDNI.Location = new System.Drawing.Point(16, 262);
            this.txtResultDNI.Name = "txtResultDNI";
            this.txtResultDNI.ReadOnly = true;
            this.txtResultDNI.Size = new System.Drawing.Size(207, 23);
            this.txtResultDNI.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 337);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tipo de Cambio";
            // 
            // txtTC
            // 
            this.txtTC.Location = new System.Drawing.Point(16, 449);
            this.txtTC.Name = "txtTC";
            this.txtTC.ReadOnly = true;
            this.txtTC.Size = new System.Drawing.Size(203, 23);
            this.txtTC.TabIndex = 1;
            // 
            // btnConsultarTC
            // 
            this.btnConsultarTC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsultarTC.Location = new System.Drawing.Point(16, 365);
            this.btnConsultarTC.Name = "btnConsultarTC";
            this.btnConsultarTC.Size = new System.Drawing.Size(87, 63);
            this.btnConsultarTC.TabIndex = 6;
            this.btnConsultarTC.Text = "Consultar";
            this.btnConsultarTC.UseVisualStyleBackColor = true;
            this.btnConsultarTC.Click += new System.EventHandler(this.btnConsultarTC_Click);
            // 
            // txtVenta
            // 
            this.txtVenta.Location = new System.Drawing.Point(120, 405);
            this.txtVenta.Name = "txtVenta";
            this.txtVenta.ReadOnly = true;
            this.txtVenta.Size = new System.Drawing.Size(99, 23);
            this.txtVenta.TabIndex = 1;
            // 
            // txtCompra
            // 
            this.txtCompra.Location = new System.Drawing.Point(120, 365);
            this.txtCompra.Name = "txtCompra";
            this.txtCompra.ReadOnly = true;
            this.txtCompra.Size = new System.Drawing.Size(99, 23);
            this.txtCompra.TabIndex = 1;
            // 
            // frmConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 521);
            this.Controls.Add(this.txtCompra);
            this.Controls.Add(this.txtVenta);
            this.Controls.Add(this.btnConsultarTC);
            this.Controls.Add(this.txtTC);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtResultDNI);
            this.Controls.Add(this.txtDNI);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnConsultaDNI);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txtcapcha);
            this.Controls.Add(this.pbxActualizar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbxImageRUC);
            this.Controls.Add(this.txtRUC);
            this.Controls.Add(this.btnConsultar);
            this.Name = "frmConsulta";
            this.Text = "SUNAT";
            this.Load += new System.EventHandler(this.btnConsultaRUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxImageRUC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxActualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.TextBox txtRUC;
        private System.Windows.Forms.PictureBox pbxImageRUC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbxActualizar;
        private System.Windows.Forms.TextBox txtcapcha;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnConsultaDNI;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDNI;
        private System.Windows.Forms.TextBox txtResultDNI;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTC;
        private System.Windows.Forms.Button btnConsultarTC;
        private System.Windows.Forms.TextBox txtVenta;
        private System.Windows.Forms.TextBox txtCompra;
    }
}

