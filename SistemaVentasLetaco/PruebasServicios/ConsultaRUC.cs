﻿using System;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Drawing;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ServiciosConsultaLetaco
{

    public class ConsultaRUC
    {
        public enum Result
        {
            Ok = 0,
            NoResul = 1,
            ErrorCapcha = 2,
            xError = 3
        }

        CookieContainer myCookie;
        /*string contexto;
        string accion;
        string modo;
        string rbtnTipo;
        string tipdoc;
        Result estado;
        string respuesta;*/
        bool development;

        public ConsultaRUC()
        {
            this.myCookie = new CookieContainer();
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.SystemDefault;
            /*this.contexto = "ti-it";
            this.accion = "consPorRuc";
            this.modo = "1";
            this.rbtnTipo = "1";
            this.tipdoc = "1";*/
            this.development = true;
        }

        private bool ValidarCertificado(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (development) return true;
            return sslPolicyErrors == SslPolicyErrors.None;
        }

        /// <summary>
        /// Devuelve un listado con los datos SUNAT de RUC consultado
        /// </summary>
        /// <param name="RUC">RUC a buscar</param>
        /// <param name="captcha">texto de imagen capcha</param>
        /// <returns>Tabla de datos de SUNAT con 2 columnas (Campo-Valor)</returns>
        /// EJEMPLOS 17157631837,15504676355
       public EntidadSUNAT DatosSunat(string RUC)
        {
            EntidadSUNAT entidadSUNAT = new EntidadSUNAT();
            try
            {
                string myurl = string.Format("https://simplefact.pe/ruc?q={0}", RUC);
                HttpWebRequest myhttpWebRequest = HttpWebRequest.CreateHttp(myurl);
                myhttpWebRequest.Proxy = null;
                myhttpWebRequest.Credentials = CredentialCache.DefaultCredentials;
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myhttpWebRequest.GetResponse();
                Stream mystream = myHttpWebResponse.GetResponseStream();
                StreamReader mystreamReader = new StreamReader(mystream);
                string xdatos = HttpUtility.HtmlDecode(mystreamReader.ReadToEnd());

                string[] xdatos2 = xdatos.Split("<table class=" + '"' + "table" + '"' + ">");
                string[] xdatos3 = xdatos2[1].Split("</table>");
                xdatos3[0] = xdatos3[0].Replace("<tbody>", "");
                xdatos3[0] = xdatos3[0].Replace("</tbody>", "");
                xdatos3[0] = xdatos3[0].Replace("<tr>", "<T>");
                xdatos3[0] = xdatos3[0].Replace("</tr>", "<T>");
                xdatos3[0] = xdatos3[0].Replace("<T> <T>", "<A>");
                xdatos3[0] = xdatos3[0].Replace("<T>  <T>", "<A>");
                xdatos3[0] = xdatos3[0].Replace("<T>", "");
                xdatos3[0] = xdatos3[0].Replace("</th> <td>", "<S>");
                xdatos3[0] = xdatos3[0].Replace("<th>", "");
                xdatos3[0] = xdatos3[0].Replace("</td>", "");
                xdatos3[0] = xdatos3[0].Replace("<br/>", "");

                string[] xdatos4 = xdatos3[0].Split("<A>");

                
                List<string> campos = new List<string>() {"RUC","RAZÓN SOCIAL","NOMBRE COMERCIAL","ESTADO","CONDICIÓN","TIPO",
                    "FECHA DE INSCRIPCIÓN","DIRECCIÓN","REGIÓN","PROVINCIA","DISTRITO","SISTEMA DE EMISIÓN","ACTIVIDAD EXTERIOR",
                    "SISTEMA DE CONTABILIDAD","EMISIÓN ELECTRÓNICA","CPE ELECTRÓNICOS","ACTIVIDADES ECONÓMICAS","PADRONES","PLE","FECHA DE BAJA"};

                foreach (string item in xdatos4)
                {
                    string[] xdatos5 = item.Split("<S>");
                    xdatos5[0] = xdatos5[0].ToUpper().Trim();
                    string valorcampo = xdatos5[1].Trim().ToUpper();

                    if (xdatos5[0].Contains(campos[0]))
                    {
                        entidadSUNAT.RUC = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[1]))
                    {
                        entidadSUNAT.RazonSocial = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[2]))
                    {
                        entidadSUNAT.NombreComercial = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[3]))
                    {
                        entidadSUNAT.Estado = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[4]))
                    {
                        entidadSUNAT.Condicion = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[5]))
                    {
                        entidadSUNAT.TipoContribuyente = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[6]))
                    {
                        entidadSUNAT.FechaInscripcion = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[7]))
                    {
                        entidadSUNAT.DomicilioFiscal = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[8]))
                    {
                        entidadSUNAT.Region = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[9]))
                    {
                        entidadSUNAT.Provincia = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[10]))
                    {
                        entidadSUNAT.Distrito = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[11]))
                    {
                        entidadSUNAT.SistEmision = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[12]))
                    {
                        entidadSUNAT.ActividadEsxterior = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[13]))
                    {
                        entidadSUNAT.SistContabilidad = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[14]))
                    {
                        entidadSUNAT.SistEmisionElectronica = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[15]))
                    {
                        entidadSUNAT.CPEElectronico = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[16]))
                    {
                        string[] x = valorcampo.Split("-");
                        entidadSUNAT.ActividadEconomica = x[1].Replace("CIIU", string.Empty).Trim();
                        entidadSUNAT.ActividadEconomicaDescripcion = x[2].Trim().ToUpper();
                    }
                    if (xdatos5[0].Contains(campos[17]))
                    {
                        entidadSUNAT.Padrones = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[18]))
                    {
                        entidadSUNAT.AfiliadoPLE = valorcampo;
                    }
                    if (xdatos5[0].Contains(campos[19]))
                    {
                        entidadSUNAT.FechaBaja = valorcampo;
                    }
                }

                entidadSUNAT.resultado = "1";
                entidadSUNAT.mensaje = "Búsqueda realizado con éxito";                
            }
            catch (Exception ex)
            {
                entidadSUNAT.resultado = "-1";
                entidadSUNAT.mensaje = ex.Message;
            }

            return entidadSUNAT;
        }
    }

    public class EntidadSUNAT
    {
        public string RUC { get; set; }
        public string RazonSocial { get; set; }
        public string TipoContribuyente { get; set; }
        public string NombreComercial { get; set; }
        public string FechaInscripcion { get; set; }
        public string FechaInicioActividades { get; set; }
        public string FechaBaja { get; set; }
        public string Estado { get; set; }
        public string Condicion { get; set; }
        public string DomicilioFiscal { get; set; }
        public string Region { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string ActividadEconomica { get; set; }
        public string ActividadEconomicaDescripcion { get; set; }
        public string ActividadEsxterior { get; set; }
        public string SistEmision { get; set; }
        public string SistEmisionElectronica { get; set; }
        public string SistContabilidad { get; set; }
        public string AfiliadoPLE { get; set; }
        public string Padrones { get; set; }
        public string CPEElectronico { get; set; }
        public string resultado { get; set; }
        public string mensaje { get; set; }
    }
}
