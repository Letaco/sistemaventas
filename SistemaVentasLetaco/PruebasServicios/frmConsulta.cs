﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServiciosConsultaLetaco
{
    public partial class frmConsulta : Form
    {
        ConsultaRUC _consultaRUC;
        ConsultaDNI _consultaDNI;
        ConsultaTC _consultaTC;
        public frmConsulta()
        {
            InitializeComponent();
        }

        private void btnConsultaRUC_Load(object sender, EventArgs e)
        {
            _consultaRUC = new ConsultaRUC();
            _consultaDNI = new ConsultaDNI();
            _consultaTC = new ConsultaTC();
        }

        private void pbxActualizar_Click(object sender, EventArgs e)
        {
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            // dataGridView1.DataSource = _consultaRUC.DatosSunat_datatable(txtRUC.Text, txtcapcha.Text);
            dataGridView1.DataSource = _consultaRUC.DatosSunat(txtRUC.Text);
            txtRUC.Clear();
            pbxActualizar_Click(sender, e);
        }

        private void btnConsultaDNI_Click(object sender, EventArgs e)
        {            
            txtResultDNI.Text = _consultaDNI.DatosReniec_Solo_nombre(txtDNI.Text);
        }

        private void btnConsultarTC_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = _consultaTC.DatosTC_SBS();
            txtTC.Text = _consultaTC.getRespuesta();

            DataTable _dataTable = _consultaTC.DatosTC_SUNAT();
            txtCompra.Text = _dataTable.Rows[0][1].ToString();
            txtVenta.Text = _dataTable.Rows[1][1].ToString();
        }
    }
}
