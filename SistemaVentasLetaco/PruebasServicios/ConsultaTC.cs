﻿using System;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ServiciosConsultaLetaco
{
    public class ConsultaTC
    {
        public enum Result
        {
            Ok = 0,
            NoResul = 1,
            ErrorCapcha = 2,
            xError = 3
        }

        CookieContainer myCookie;
        bool development;
        string respuesta;
        Result estado;

        public ConsultaTC() {
            //this.myCookie = new CookieContainer();
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.SystemDefault;
            this.development = true;
        }

        private bool ValidarCertificado(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (development) return true;
            return sslPolicyErrors == SslPolicyErrors.None;
        }

        public string getRespuesta()
        {
            return this.respuesta;
        }
        public int getEstado()
        {
            if (this.estado == Result.Ok)
                return 1;
            else
                return 0;
        }


        /// <summary>
        /// Lista los tipos de cambio de diversas monedas (Datos tomados de la SBS)
        /// Dato de última fecha laboral
        /// </summary>
        /// <returns>Tabla de datos con 3 columnas (Moneda-Compra-Venta)</returns>
        public DataTable DatosTC_SBS()
        {
            try
            {                
                string myurl = "https://www.sbs.gob.pe/app/pp/sistip_portal/paginas/publicacion/tipocambiopromedio.aspx?";
                myurl += "ctl00$cphContent$btnConsultar={0}";
                myurl = myurl.Replace("{0}", "consultar");

                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidarCertificado);
                HttpWebRequest myhttpWebRequest = HttpWebRequest.CreateHttp(myurl);
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myhttpWebRequest.GetResponse();
                Stream mystream = myHttpWebResponse.GetResponseStream();                
                StreamReader mystreamReader = new StreamReader(mystream);
                string xdatos = HttpUtility.HtmlDecode(mystreamReader.ReadToEnd());
                
                string[] xdatos2 = xdatos.Split("</thead><tbody>");
                string[] xdatos3 = xdatos2.GetValue(5).ToString().Split("</tbody>");
                string xdatos4 = xdatos3.GetValue(0).ToString();
                xdatos4 = xdatos4.Replace("\n", "");
                xdatos4 = xdatos4.Replace("\r", "");
                xdatos4 = xdatos4.Replace("\t", "");
                xdatos4 = xdatos4.Replace("<tr class=\"rgRow\" id=\"ctl00_cphContent_rgTipoCambio_ctl00__0\">", "");

                string xclass = "rgRow";
                for (int i = 1; i < 7; i++)
                {
                    if ((i % 2) == 0)
                        xclass = "rgRow";
                    else
                        xclass = "rgAltRow";

                    xdatos4 = xdatos4.Replace("</tr><tr class=\"" + xclass +"\" id=\"ctl00_cphContent_rgTipoCambio_ctl00__" + i.ToString() +"\">", "separar");                    
                }

                string[] xdatos5 = xdatos4.Trim().Split("separar");

                for (int i = 0; i < xdatos5.Length; i++)
                {
                    string cadena = xdatos5.GetValue(i).ToString();
                    cadena = cadena.Replace("<td class=\"APLI_fila3\" width=\"40%\">", "");
                    cadena = cadena.Replace("</td>", string.Empty);
                    cadena = cadena.Replace("</tr>", string.Empty);                    
                    cadena = cadena.Replace("<td class=\"APLI_fila2\" width=\"30%\">", "XR");
                    xdatos5.SetValue(cadena, i);
                }

                DataTable dataTableResponse = new DataTable();
                dataTableResponse.Columns.Add("Moneda Extranjera");
                dataTableResponse.Columns.Add("Valor Compra Soles");
                dataTableResponse.Columns.Add("Valor Venta Soles");

                dataTableResponse.Columns[0].Caption = "Moneda Extranjera";
                dataTableResponse.Columns[1].Caption = "Valor Compra Soles";
                dataTableResponse.Columns[2].Caption = "Valor Venta Soles";
                dataTableResponse.Columns[0].ColumnName = "MONEDA_ORIGINAL";
                dataTableResponse.Columns[1].ColumnName = "COMPRA_SOLES";
                dataTableResponse.Columns[2].ColumnName = "VENTA_SOLES";

                foreach (var item in xdatos5)
                {
                    string[] contenido = item.Split("XR");

                    DataRow dataRowNew = dataTableResponse.NewRow();
                    dataRowNew[0] = contenido.GetValue(0).ToString();
                    dataRowNew[1] = contenido.GetValue(1).ToString();
                    dataRowNew[2] = contenido.GetValue(2).ToString();

                    dataTableResponse.Rows.Add(dataRowNew);
                }
              
                this.respuesta = "Datos Obtenidos correctamente";
                this.estado = Result.Ok;

                return dataTableResponse;

            }
            catch (Exception ex)
            {
                this.respuesta = ex.Message;
                this.estado = Result.xError;
                return null;
            }

        }

        /// <summary>
        /// Devuelve el Tipo de Cambio por fecha (Datos tomados de SUNAT)
        /// </summary>
        /// <param name="fecha">fecha de consulta (si no se coloca fecha se tomará la de hoy)</param>
        /// <returns>Tabla de datos con 2 columnas (Concepto-Valor)</returns>
        public DataTable DatosTC_SUNAT(string fecha = "")
        {
            try
            {
                string myurl = "https://dni.optimizeperu.com/api/tipo-cambio";
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidarCertificado);
                HttpWebRequest myhttpWebRequest = HttpWebRequest.CreateHttp(myurl);
                myhttpWebRequest.Headers.Add("Authorization", "Bearer k4d2956bd531ab61d44f4fa07304b20e13913815");
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myhttpWebRequest.GetResponse();
                Stream mystream = myHttpWebResponse.GetResponseStream();
                StreamReader mystreamReader = new StreamReader(mystream);

                string xdatos = HttpUtility.HtmlDecode(mystreamReader.ReadToEnd());

                TipoCambioGeneral _tipoCambioGeneral = (TipoCambioGeneral)JsonConvert.DeserializeObject(xdatos, typeof(TipoCambioGeneral));
                
                this.respuesta = "Datos Obtenidos correctamente";
                this.estado = Result.Ok;

                DataTable dataTableResponse = new DataTable();
                dataTableResponse.Columns.Add("Concepto");
                dataTableResponse.Columns.Add("Valor");

                string compra = "", venta = "";
                if (fecha == string.Empty)
                {
                    compra = _tipoCambioGeneral.cambio_actual.compra;
                    venta = _tipoCambioGeneral.cambio_actual.venta;
                }
                else
                {
                    string[] xfecha = fecha.Split("/");

                    foreach (EntidadDia entidadDia in _tipoCambioGeneral.dias)
                    {
                        if (int.Parse(xfecha.GetValue(0).ToString()) == int.Parse(entidadDia.dia))
                        {
                            compra = entidadDia.compra;
                            venta = entidadDia.venta;
                        }
                    }

                }

                DataRow dataRowNew = dataTableResponse.NewRow();
                dataRowNew[0] = "Compra";
                dataRowNew[1] = compra;
                dataTableResponse.Rows.Add(dataRowNew);
                dataRowNew = dataTableResponse.NewRow();
                dataRowNew[0] = "Venta";
                dataRowNew[1] = venta;
                dataTableResponse.Rows.Add(dataRowNew);

                return dataTableResponse;
            }
            catch (Exception ex)
            {
                this.respuesta = ex.Message;
                this.estado = Result.xError;
                return null;
            }

        }

    }

    public class TipoCambioGeneral {
        public EntidadAnio anio { get; set; }
        public EntidadDia cambio_actual { get; set; }
        public List<EntidadDia> dias { get; set; }
    }

    public class EntidadDia {
        public string dia { get; set; }
        public string compra { get; set; }
        public string venta { get; set; }
    }

    public class EntidadAnio{
        public string mes_nombre { get; set; }
        public string mes_valor { get; set; }
        public string anio { get; set; }
    }

}
