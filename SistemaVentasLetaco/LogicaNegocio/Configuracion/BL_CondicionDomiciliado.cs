﻿using System;
using System.Collections.Generic;
using Entidades;
using EnlaceDatos;


namespace LogicaNegocio
{
    public class BL_CondicionDomiciliado
    {
        public BL_CondicionDomiciliado() { }

        public Respuesta Insertar(params object[] valoresPermitidos)
        {
            Respuesta respuesta;

            try
            {
                DAO_CondicionDomiciliado _CondicionDomiciliado = new DAO_CondicionDomiciliado();

                ENT_CondicionDomiciliado eNT_CondicionDomiciliado  = (ENT_CondicionDomiciliado)valoresPermitidos[0];

                if (string.IsNullOrEmpty(eNT_CondicionDomiciliado.descripcion))
                {
                    respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ListAtributos.msj_respuestaErrorValidacionBlancos };
                }
                else
                    respuesta = _CondicionDomiciliado.Insertar(valoresPermitidos);
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            return respuesta;

        }

        public Respuesta Actualizar(params object[] valoresPermitidos)
        {
            Respuesta respuesta;

            try
            {
                DAO_CondicionDomiciliado _CondicionDomiciliado = new DAO_CondicionDomiciliado();

                ENT_CondicionDomiciliado eNT_CondicionDomiciliado = (ENT_CondicionDomiciliado)valoresPermitidos[0];

                if (string.IsNullOrEmpty(eNT_CondicionDomiciliado.descripcion))
                {
                    respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ListAtributos.msj_respuestaErrorValidacionBlancos };
                }
                else
                    respuesta = _CondicionDomiciliado.Actualizar(valoresPermitidos);
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            return respuesta;

        }

        public List<ENT_CondicionDomiciliado> Listar(params object[] valoresPermitidos) 
        {
            Respuesta respuesta;
            List<ENT_CondicionDomiciliado> eNT_CondicionDomiciliadoListResponse = new List<ENT_CondicionDomiciliado>();
            ENT_CondicionDomiciliado eNT_CondicionDomiciliado = (ENT_CondicionDomiciliado)valoresPermitidos[0];

            try
            {

                DAO_CondicionDomiciliado _CondicionDomiciliado = new DAO_CondicionDomiciliado();                

                if (string.IsNullOrEmpty(eNT_CondicionDomiciliado.descripcion))
                {
                    respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ListAtributos.msj_respuestaErrorValidacionBlancos };
                    eNT_CondicionDomiciliado.Respuesta = respuesta;
                    eNT_CondicionDomiciliadoListResponse.Add(eNT_CondicionDomiciliado);
                }
                else
                    eNT_CondicionDomiciliadoListResponse = _CondicionDomiciliado.Listar(valoresPermitidos);
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message};
                eNT_CondicionDomiciliado.Respuesta = respuesta;
                eNT_CondicionDomiciliadoListResponse.Add(eNT_CondicionDomiciliado);
            }

            return eNT_CondicionDomiciliadoListResponse;
        }
    }
}
