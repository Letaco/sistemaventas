﻿using System;
using System.Collections.Generic;
using Entidades;
using EnlaceDatos;

namespace LogicaNegocio
{
    public class BL_TipoDocIdentidad
    {
        public BL_TipoDocIdentidad() { }


        public List<ENT_TipoDocIdentidad> ListarTipoDocIdentidadCombo(params object[] valoresPermitidos)
        {
            Respuesta respuesta;
            List<ENT_TipoDocIdentidad> eNT_TipoDocIdentidadListResponse = new List<ENT_TipoDocIdentidad>();
            ENT_TipoDocIdentidad eNT_TipoDocIdentidad = (ENT_TipoDocIdentidad)valoresPermitidos[0];

            try
            {
                DAO_TipoDocIdentidad _TipoDocIdentidad = new DAO_TipoDocIdentidad();

                eNT_TipoDocIdentidadListResponse = _TipoDocIdentidad.ListarTipoDocIdentidad(valoresPermitidos);

            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
                eNT_TipoDocIdentidad.Respuesta = respuesta;
                eNT_TipoDocIdentidadListResponse.Add(eNT_TipoDocIdentidad);
            }

            return eNT_TipoDocIdentidadListResponse;
        }
    }
}
