﻿using System;
using System.Collections.Generic;
using Entidades;
using EnlaceDatos;

namespace LogicaNegocio
{
    public class BL_TipoActividad
    {
        public BL_TipoActividad() { }

        public List<ENT_TipoActividad> ListarTipoActividadCombo(params object[] valoresPermitidos)
        {
            Respuesta respuesta;
            List<ENT_TipoActividad> eNT_TipoActividadListResponse = new List<ENT_TipoActividad>();
            ENT_TipoActividad eNT_TipoActividad = (ENT_TipoActividad)valoresPermitidos[0];

            try
            {
                DAO_TipoActividad _TipoActividad = new DAO_TipoActividad();

                eNT_TipoActividadListResponse = _TipoActividad.ListarComboTipoActividad(valoresPermitidos);

            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
                eNT_TipoActividad.Respuesta = respuesta;
                eNT_TipoActividadListResponse.Add(eNT_TipoActividad);
            }

            return eNT_TipoActividadListResponse;
        }
    }
}
