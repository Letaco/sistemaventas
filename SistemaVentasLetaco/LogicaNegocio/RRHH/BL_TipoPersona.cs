﻿using System;
using System.Collections.Generic;
using Entidades;
using EnlaceDatos;

namespace LogicaNegocio
{
    public class BL_TipoPersona
    {
        public BL_TipoPersona() { }

        public List<ENT_TipoPersona> ListarTipoPersonaCombo(params object[] valoresPermitidos)
        {
            Respuesta respuesta;
            List<ENT_TipoPersona> eNT_TipoPersonaListResponse = new List<ENT_TipoPersona>();
            ENT_TipoPersona eNT_TipoPersona = (ENT_TipoPersona)valoresPermitidos[0];

            try
            {
                DAO_TipoPersona _TipoPersona = new DAO_TipoPersona();

                eNT_TipoPersonaListResponse = _TipoPersona.ListarComboTipoPersona(valoresPermitidos);

            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
                eNT_TipoPersona.Respuesta = respuesta;
                eNT_TipoPersonaListResponse.Add(eNT_TipoPersona);
            }

            return eNT_TipoPersonaListResponse;
        }
    }
}
