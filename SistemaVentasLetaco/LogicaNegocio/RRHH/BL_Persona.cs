﻿using System;
using System.Collections.Generic;
using Entidades;
using EnlaceDatos;

namespace LogicaNegocio
{
    public class BL_Persona
    {
        public BL_Persona() { }

        public List<ENT_Persona> ListarPersonaCombo(params object[] valoresPermitidos)
        {
            Respuesta respuesta;
            List<ENT_Persona> eNT_PersonaListResponse = new List<ENT_Persona>();
            ENT_Persona eNT_Persona = (ENT_Persona)valoresPermitidos[0];

            try
            {
                DAO_Persona _Persona = new DAO_Persona();

                eNT_PersonaListResponse = _Persona.ListarPersonaCombo(valoresPermitidos);

            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
                eNT_Persona.Respuesta = respuesta;
                eNT_PersonaListResponse.Add(eNT_Persona);
            }

            return eNT_PersonaListResponse;
        }

        public Respuesta Insertar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();

            try
            {
                DAO_Persona _Persona = new DAO_Persona();

                ENT_Persona eNT_Persona = (ENT_Persona)valoresPermitidos[0];

                if (string.IsNullOrEmpty(eNT_Persona.nombres_razSoc) || string.IsNullOrEmpty(eNT_Persona.apellidos_nomCom))
                {
                    respuesta.idRespuesta = -1;
                    respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorValidacionBlancos;
                }
                else
                    respuesta = _Persona.Insertar(valoresPermitidos);
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            return respuesta;
        }


    }
}
