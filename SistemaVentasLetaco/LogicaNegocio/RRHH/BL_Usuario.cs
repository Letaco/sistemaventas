﻿using System;
using System.Collections.Generic;
using Entidades;
using EnlaceDatos;

namespace LogicaNegocio
{
    public class BL_Usuario
    {
        public BL_Usuario() { }

        public Respuesta Insertar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();

            try
            {
                DAO_Usuario _Usuario = new DAO_Usuario();

                ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];

                if (string.IsNullOrEmpty(eNT_Usuario.usuario) || string.IsNullOrEmpty(eNT_Usuario.clave))
                {
                    respuesta.idRespuesta = -1;
                    respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorValidacionBlancos;
                }
                else
                    respuesta = _Usuario.Insertar(valoresPermitidos);
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }

            return respuesta;
        }

        public Respuesta Editar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();

            try
            {
                DAO_Usuario _Usuario = new DAO_Usuario();

                ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];

                if (string.IsNullOrEmpty(eNT_Usuario.usuario) || string.IsNullOrEmpty(eNT_Usuario.clave))
                {
                    respuesta.idRespuesta = -1;
                    respuesta.mensajeRespuesta = ListAtributos.msj_respuestaErrorValidacionBlancos;
                }
                else
                    respuesta = _Usuario.Editar(valoresPermitidos);
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }

            return respuesta;
        }

        public List<ENT_Usuario> Listar(params object[] valoresPermitidos)
        {
            Respuesta respuesta;
            List<ENT_Usuario> eNT_UsuarioListResponse = new List<ENT_Usuario>();
            ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];

            try
            {
                DAO_Usuario _Usuario = new DAO_Usuario();

                eNT_UsuarioListResponse = _Usuario.Listar(valoresPermitidos);

            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
                eNT_Usuario.respuestaTransaccion = respuesta;
                eNT_UsuarioListResponse.Add(eNT_Usuario);
            }
            return eNT_UsuarioListResponse;
        }

        public List<ENT_Usuario> ListarUsuarioCombo(params object[] valoresPermitidos)
        {
            Respuesta respuesta;
            List<ENT_Usuario> eNT_UsuarioListResponse = new List<ENT_Usuario>();
            ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];

            try
            {
                DAO_Usuario _Usuario = new DAO_Usuario();

                eNT_UsuarioListResponse = _Usuario.ListarUsuarioCombo(valoresPermitidos);

            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
                eNT_Usuario.respuestaTransaccion = respuesta;
                eNT_UsuarioListResponse.Add(eNT_Usuario);
            }
            return eNT_UsuarioListResponse;
        }

        public ENT_Usuario IniciarSesion(params object[] valoresPermitidos)
        {
            Respuesta respuesta;
            ENT_Usuario eNT_UsuarioResponse = new ENT_Usuario();
            ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];

            try
            {
                DAO_Usuario _Usuario = new DAO_Usuario();
                eNT_UsuarioResponse = _Usuario.IniciarSesion(eNT_Usuario);
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
                eNT_UsuarioResponse.respuestaTransaccion = respuesta;
            }
            return eNT_UsuarioResponse;
        }

        public Respuesta Eliminar(params object[] valoresPermitidos)
        {
            Respuesta respuesta = new Respuesta();

            try
            {
                DAO_Usuario _Usuario = new DAO_Usuario();

                ENT_Usuario eNT_Usuario = (ENT_Usuario)valoresPermitidos[0];

                respuesta = _Usuario.Eliminar(valoresPermitidos);

            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            return respuesta;
        }

    }
}
