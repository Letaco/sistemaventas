﻿using System;
using System.Collections.Generic;
using System.Text;
using Entidades;
using EntidadesIntermedias;
using LogicaNegocio;

namespace EnlacePresentacion
{
    public class EPBL_TipoActividad
    {
        public EPBL_TipoActividad() { }

        public TipoActividadResponse ListarTipoActividadCombo(TipoActividadRequest tipoActividadRequest)
        {
            Respuesta respuesta;
            TipoActividadResponse tipoActividadResponse = new TipoActividadResponse();

            try
            {
                BL_TipoActividad _TipoActividad = new BL_TipoActividad();

                tipoActividadResponse.eNT_TipoActividadList = _TipoActividad.ListarTipoActividadCombo(tipoActividadRequest.eNT_TipoActividad);

                string mensaje = (tipoActividadResponse.eNT_TipoActividadList.Count == 0) ? Generales.msj_ListaVacia : tipoActividadResponse.eNT_TipoActividadList[0].Respuesta.mensajeRespuesta;

                respuesta = new Respuesta() { idRespuesta = tipoActividadResponse.eNT_TipoActividadList.Count, mensajeRespuesta = mensaje };
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            tipoActividadResponse.respuestaTransaccion = respuesta;

            return tipoActividadResponse;
        }
    }
}
