﻿using System;
using System.Collections.Generic;
using System.Text;
using Entidades;
using EntidadesIntermedias;
using LogicaNegocio;

namespace EnlacePresentacion
{
    public class EPBL_Persona
    {
        public EPBL_Persona() { }

        public PersonaResponse ListarPersonaCombo(PersonaRequest personaRequest)
        {
            Respuesta respuesta;
            PersonaResponse personaResponse = new PersonaResponse();

            try
            {
                BL_Persona _Persona = new BL_Persona();

                personaResponse.eNT_PersonaList = _Persona.ListarPersonaCombo(personaRequest.eNT_Persona, personaRequest.tipoListado);

                string mensaje = (personaResponse.eNT_PersonaList.Count == 0) ? Generales.msj_ListaVacia : personaResponse.eNT_PersonaList[0].Respuesta.mensajeRespuesta;

                respuesta = new Respuesta() { idRespuesta = personaResponse.eNT_PersonaList.Count, mensajeRespuesta = mensaje };
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            personaResponse.respuestaTransaccion = respuesta;

            return personaResponse;
        }

        public PersonaResponse Insertar(PersonaRequest personaRequest)
        {
            Respuesta respuesta = new Respuesta();
            PersonaResponse personaResponse = new PersonaResponse();

            try
            {
                BL_Persona _Persona = new BL_Persona();
                respuesta = _Persona.Insertar(personaRequest.eNT_Persona);
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            personaResponse.respuestaTransaccion = respuesta;

            return personaResponse;
        }


    }
}
