﻿using System;
using System.Collections.Generic;
using System.Text;
using Entidades;
using EntidadesIntermedias;
using LogicaNegocio;

namespace EnlacePresentacion
{
    public class EPBL_TipoPersona
    {
        public EPBL_TipoPersona() { }

        public TipoPersonaResponse ListarTipoPersonaCombo(TipoPersonaRequest tipoPersonaRequest)
        {
            Respuesta respuesta;
            TipoPersonaResponse tipoPersonaResponse = new TipoPersonaResponse();

            try
            {
                BL_TipoPersona _TipoPersona = new BL_TipoPersona();

                tipoPersonaResponse.eNT_TipoPersonaList = _TipoPersona.ListarTipoPersonaCombo(tipoPersonaRequest.eNT_TipoPersona);

                string mensaje = (tipoPersonaResponse.eNT_TipoPersonaList.Count == 0) ? Generales.msj_ListaVacia : tipoPersonaResponse.eNT_TipoPersonaList[0].Respuesta.mensajeRespuesta;

                respuesta = new Respuesta() { idRespuesta = tipoPersonaResponse.eNT_TipoPersonaList.Count, mensajeRespuesta = mensaje };
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            tipoPersonaResponse.respuestaTransaccion = respuesta;

            return tipoPersonaResponse;
        }
    }
}
