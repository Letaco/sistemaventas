﻿using System;
using System.Collections.Generic;
using Entidades;
using EntidadesIntermedias;
using LogicaNegocio;

namespace EnlacePresentacion
{
    public class EPBL_Usuario
    {
        public EPBL_Usuario() { }

        public UsuarioResponse Insertar(UsuarioRequest usuarioRequest)
        {
            Respuesta respuesta = new Respuesta();
            UsuarioResponse usuarioResponse = new UsuarioResponse();

            try
            {
                BL_Usuario _Usuario = new BL_Usuario();
                respuesta = _Usuario.Insertar(usuarioRequest.eNT_Usuario);
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }

            usuarioResponse.respuestaTransaccion = respuesta;

            return usuarioResponse;
        }

        public UsuarioResponse Editar(UsuarioRequest usuarioRequest)
        {
            Respuesta respuesta = new Respuesta();
            UsuarioResponse usuarioResponse = new UsuarioResponse();

            try
            {
                BL_Usuario _Usuario = new BL_Usuario();
                respuesta = _Usuario.Editar(usuarioRequest.eNT_Usuario);
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }

            usuarioResponse.respuestaTransaccion = respuesta;

            return usuarioResponse;
        }

        public UsuarioResponse Listar(UsuarioRequest UsuarioRequest)
        {
            Respuesta respuesta;
            UsuarioResponse usuarioResponse = new UsuarioResponse();

            try
            {
                BL_Usuario _Usuario = new BL_Usuario();

                usuarioResponse.eNT_UsuarioList = _Usuario.Listar(UsuarioRequest.eNT_Usuario);

                string mensaje = (usuarioResponse.eNT_UsuarioList.Count == 0) ? Generales.msj_ListaVacia : usuarioResponse.eNT_UsuarioList[0].respuestaTransaccion.mensajeRespuesta;

                respuesta = new Respuesta() { idRespuesta = usuarioResponse.eNT_UsuarioList.Count, mensajeRespuesta = mensaje };
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            usuarioResponse.respuestaTransaccion = respuesta;

            return usuarioResponse;
        }

        public UsuarioResponse ListarUsuarioCombo(UsuarioRequest UsuarioRequest)
        {
            Respuesta respuesta;
            UsuarioResponse usuarioResponse = new UsuarioResponse();

            try
            {
                BL_Usuario _Usuario = new BL_Usuario();

                usuarioResponse.eNT_UsuarioList = _Usuario.ListarUsuarioCombo(UsuarioRequest.eNT_Usuario,UsuarioRequest.tipoListado);

                string mensaje = (usuarioResponse.eNT_UsuarioList.Count == 0) ? Generales.msj_ListaVacia : usuarioResponse.eNT_UsuarioList[0].respuestaTransaccion.mensajeRespuesta;

                respuesta = new Respuesta() { idRespuesta = usuarioResponse.eNT_UsuarioList.Count, mensajeRespuesta = mensaje };
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            usuarioResponse.respuestaTransaccion = respuesta;

            return usuarioResponse;
        }

        public UsuarioResponse Eliminar(UsuarioRequest usuarioRequest)
        {
            Respuesta respuesta = new Respuesta();
            UsuarioResponse usuarioResponse = new UsuarioResponse();

            try
            {
                BL_Usuario _Usuario = new BL_Usuario();
                respuesta = _Usuario.Eliminar(usuarioRequest.eNT_Usuario);
            }
            catch (Exception ex)
            {
                respuesta.idRespuesta = -1;
                respuesta.mensajeRespuesta = ex.Message;
            }
            usuarioResponse.respuestaTransaccion = respuesta;

            return usuarioResponse;
        }

        public UsuarioResponse IniciarSesion(UsuarioRequest UsuarioRequest)
        {
            Respuesta respuesta;
            UsuarioResponse usuarioResponse = new UsuarioResponse();

            try
            {
                BL_Usuario _Usuario = new BL_Usuario();
                usuarioResponse.eNT_Usuario = _Usuario.IniciarSesion(UsuarioRequest.eNT_Usuario);

                /*
                 * Se debe agregar código para obtener el menú autorizado para el usuario
                 * Todo ese contenido simplemente se agrega a la variable response
                 */

                respuesta = usuarioResponse.eNT_Usuario.respuestaTransaccion;
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            usuarioResponse.respuestaTransaccion = respuesta;

            return usuarioResponse;
        }
    }
}
