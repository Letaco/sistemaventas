﻿using System;
using System.Collections.Generic;
using System.Text;
using Entidades;
using EntidadesIntermedias;
using LogicaNegocio;

namespace EnlacePresentacion
{
    public class EPBL_TipoDocIdentidad
    {
        public EPBL_TipoDocIdentidad() { }

        public TipoDocIdentidadResponse ListarTipoDocIdentidadCombo(TipoDocIdentidadRequest tipoDocIdentidadRequest)
        {
            Respuesta respuesta;
            TipoDocIdentidadResponse tipoDocIdentidadResponse = new TipoDocIdentidadResponse();

            try
            {
                BL_TipoDocIdentidad _TipoDocIdentidad = new BL_TipoDocIdentidad();

                tipoDocIdentidadResponse.eNT_TipoDocIdentidadList = _TipoDocIdentidad.ListarTipoDocIdentidadCombo(tipoDocIdentidadRequest.eNT_TipoDocIdentidad);
                
                string mensaje = (tipoDocIdentidadResponse.eNT_TipoDocIdentidadList.Count == 0) ? Generales.msj_ListaVacia : tipoDocIdentidadResponse.eNT_TipoDocIdentidadList[0].Respuesta.mensajeRespuesta;

                respuesta = new Respuesta() { idRespuesta = tipoDocIdentidadResponse.eNT_TipoDocIdentidadList.Count, mensajeRespuesta = mensaje };
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            tipoDocIdentidadResponse.respuestaTransaccion = respuesta;

            return tipoDocIdentidadResponse;
        }
    }
}
