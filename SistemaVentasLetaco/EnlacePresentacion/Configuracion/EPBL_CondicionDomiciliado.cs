﻿using System;
using System.Collections.Generic;
using System.Text;
using Entidades;
using EntidadesIntermedias;
using LogicaNegocio;

namespace EnlacePresentacion
{
    public class EPBL_CondicionDomiciliado
    {
        public EPBL_CondicionDomiciliado() { }

        public CondicionDomiciliadoResponse Insertar(CondicionDomiciliadoRequest condicionDomiciliadoRequest)
        {
            Respuesta respuesta;
            CondicionDomiciliadoResponse condicionDomiciliadoResponse = new CondicionDomiciliadoResponse();

            try
            {
                BL_CondicionDomiciliado _CondicionDomiciliado = new BL_CondicionDomiciliado();
                respuesta = _CondicionDomiciliado.Insertar(condicionDomiciliadoRequest.ENT_CondicionDomiciliado);
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            
            condicionDomiciliadoResponse.respuestaTransaccion = respuesta;

            return condicionDomiciliadoResponse;
        }

        public CondicionDomiciliadoResponse Actualizar(CondicionDomiciliadoRequest condicionDomiciliadoRequest)
        {
            Respuesta respuesta;
            CondicionDomiciliadoResponse condicionDomiciliadoResponse = new CondicionDomiciliadoResponse();

            try
            {
                BL_CondicionDomiciliado _CondicionDomiciliado = new BL_CondicionDomiciliado();
                respuesta = _CondicionDomiciliado.Actualizar(condicionDomiciliadoRequest.ENT_CondicionDomiciliado);
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }
            
            condicionDomiciliadoResponse.respuestaTransaccion = respuesta;

            return condicionDomiciliadoResponse;
        }

        public CondicionDomiciliadoResponse Listar(CondicionDomiciliadoRequest condicionDomiciliadoRequest)
        {
            Respuesta respuesta;
            CondicionDomiciliadoResponse condicionDomiciliadoResponse = new CondicionDomiciliadoResponse();

            try
            {
                BL_CondicionDomiciliado _CondicionDomiciliado = new BL_CondicionDomiciliado();
                
                condicionDomiciliadoResponse.eNT_CondicionDomiciliadoList = _CondicionDomiciliado.Listar(condicionDomiciliadoRequest.ENT_CondicionDomiciliado);

                string mensaje = (condicionDomiciliadoResponse.eNT_CondicionDomiciliadoList.Count == 0) ? Generales.msj_ListaVacia : condicionDomiciliadoResponse.eNT_CondicionDomiciliadoList[0].Respuesta.mensajeRespuesta;

                respuesta = new Respuesta() { idRespuesta = condicionDomiciliadoResponse.eNT_CondicionDomiciliadoList.Count, mensajeRespuesta = mensaje};
            }
            catch (Exception ex)
            {
                respuesta = new Respuesta() { idRespuesta = -1, mensajeRespuesta = ex.Message };
            }

            condicionDomiciliadoResponse.respuestaTransaccion = respuesta;

            return condicionDomiciliadoResponse;
        }
    }
}
